
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>The Tsetlin library &#8212; Thematic Tutorials</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../_static/thebelab.css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script src="../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Young’s lattice and the RSK algorithm" href="rsk.html" />
    <link rel="prev" title="\(n\)-Cube" href="n_cube.html" />
  <link rel="icon" href="../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="rsk.html" title="Young’s lattice and the RSK algorithm"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="n_cube.html" title="\(n\)-Cube"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../index.html">Thematic Tutorials</a> &#187;
    
  </li>

          <li class="nav-item nav-item-1"><a href="../toctree.html" >Thematic tutorial document tree</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="../algebraic_combinatorics.html" accesskey="U">Algebraic Combinatorics in Sage</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">The Tsetlin library</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="the-tsetlin-library">
<h1>The Tsetlin library<a class="headerlink" href="#the-tsetlin-library" title="Permalink to this headline">¶</a></h1>
<section id="introduction">
<h2>Introduction<a class="headerlink" href="#introduction" title="Permalink to this headline">¶</a></h2>
<p>In this section, we study a simple random walk (or Markov chain),
called the <em>Tsetlin library</em>. It will give us the opportunity to see
the interplay between combinatorics, linear algebra, representation
theory and computer exploration, without requiring heavy theoretical
background. I hope this encourages everyone to play around with
this or similar systems and investigate their properties!
Formal theorems and proofs can be found in the references at the
end of this section.</p>
<p>It has been known for several years that the theory of group
representations can facilitate the study of systems whose evolution
is random (Markov chains), breaking them down into simpler systems.
More recently it was realized that generalizing this (namely
replacing the invertibility axiom for groups by other axioms)
explains the behavior of other particularly simple Markov chains
such as the Tsetlin library.</p>
</section>
<section id="id1">
<h2>The Tsetlin library<a class="headerlink" href="#id1" title="Permalink to this headline">¶</a></h2>
<p>Consider a bookshelf in a library containing <span class="math notranslate nohighlight">\(n\)</span> distinct books. When
a person borrows a book and then returns it, it gets placed back on the
shelf to the right of all books. This is what we naturally do with our
pile of shirts in the closet: after use and cleaning, the shirt is placed
on the top of its pile. Hence the most popular books/shirts will more
likely appear on the right/top of the shelf/pile.</p>
<p>This type of organization has the advantage of being self-adaptive:</p>
<ul class="simple">
<li><p>The books most often used accumulate on the right and thus can easily
be found.</p></li>
<li><p>If the use changes over time, the system adapts.</p></li>
</ul>
<p>In fact, this type of strategy is used not only in everyday life, but also
in computer science. The natural questions that arise are:</p>
<ul class="simple">
<li><p><em>Stationary distribution</em>: To which state(s) does the system converge to? This,
among other things, is used to evaluate the average access time to
a book.</p></li>
<li><p><em>The rate of convergence</em>: How fast does the system adapt to a changing
environment .</p></li>
</ul>
<p>Let us formalize the description. The Tsetlin library is a discrete Markov
chain (discrete time, discrete state space) described by:</p>
<ul class="simple">
<li><p>The state space <span class="math notranslate nohighlight">\(\Omega_n\)</span> is given by the set of all permutations of the
<span class="math notranslate nohighlight">\(n\)</span> books.</p></li>
<li><p>The transition operators are denoted by <span class="math notranslate nohighlight">\(\partial_i \colon \Omega_n \to \Omega_n\)</span>.
When <span class="math notranslate nohighlight">\(\partial_i\)</span> is applied to a permutation <span class="math notranslate nohighlight">\(\sigma\)</span>, the number <span class="math notranslate nohighlight">\(i\)</span> is moved
to the end of the permutation.</p></li>
<li><p>We assign parameters <span class="math notranslate nohighlight">\(x_i \ge 0\)</span> for all <span class="math notranslate nohighlight">\(1\le i\le n\)</span> with
<span class="math notranslate nohighlight">\(\sum_{i=1}^n x_i = 1\)</span>. The parameter <span class="math notranslate nohighlight">\(x_i\)</span> indicates the probability of
choosing the operator <span class="math notranslate nohighlight">\(\partial_i\)</span>.</p></li>
</ul>
</section>
<section id="transition-graph-and-matrix">
<h2>Transition graph and matrix<a class="headerlink" href="#transition-graph-and-matrix" title="Permalink to this headline">¶</a></h2>
<p>One can depict the action of the operators <span class="math notranslate nohighlight">\(\partial_i\)</span> on the state
space <span class="math notranslate nohighlight">\(\Omega_n\)</span> by a digraph. The following picture shows the action
of <span class="math notranslate nohighlight">\(\partial_1, \partial_2, \partial_3\)</span> on <span class="math notranslate nohighlight">\(\Omega_3\)</span>:</p>
<a class="reference internal image-reference" href="../_images/tsetlin-library.png"><img alt="../_images/tsetlin-library.png" class="align-center" src="../_images/tsetlin-library.png" style="width: 274.5px; height: 354.75px;" /></a>
<p>The above picture can be reproduced in Sage as follows:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">P</span> <span class="o">=</span> <span class="n">Poset</span><span class="p">(([</span><span class="mi">1</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">3</span><span class="p">],[]))</span>
</pre></div>
</div>
<p>This is the antichain poset. Its linear extensions are all permutations of <span class="math notranslate nohighlight">\(\{1,2,3\}\)</span>:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">L</span> <span class="o">=</span> <span class="n">P</span><span class="o">.</span><span class="n">linear_extensions</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">L</span>
<span class="go">The set of all linear extensions of Finite poset containing 3 elements</span>
<span class="gp">sage: </span><span class="n">L</span><span class="o">.</span><span class="n">list</span><span class="p">()</span>
<span class="go">[[3, 2, 1], [3, 1, 2], [1, 3, 2], [1, 2, 3], [2, 1, 3], [2, 3, 1]]</span>
</pre></div>
</div>
<p>The graph is produced via:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">L</span><span class="o">.</span><span class="n">markov_chain_digraph</span><span class="p">(</span><span class="n">labeling</span><span class="o">=</span><span class="s1">&#39;source&#39;</span><span class="p">);</span> <span class="n">G</span>
<span class="go">Looped multi-digraph on 6 vertices</span>
<span class="gp">sage: </span><span class="n">view</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>  <span class="c1"># not tested</span>
</pre></div>
</div>
<p>We can now look at the transition matrix and see whether we notice anything about
its eigenvalue and eigenvectors:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">M</span> <span class="o">=</span> <span class="n">L</span><span class="o">.</span><span class="n">markov_chain_transition_matrix</span><span class="p">(</span><span class="n">labeling</span><span class="o">=</span><span class="s1">&#39;source&#39;</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">M</span>
<span class="go">[-x0 - x1       x2        0        0       x2        0]</span>
<span class="go">[      x1 -x0 - x2       x1        0        0        0]</span>
<span class="go">[       0        0 -x0 - x1       x2        0       x2]</span>
<span class="go">[      x0        0       x0 -x1 - x2        0        0]</span>
<span class="go">[       0        0        0       x1 -x0 - x2       x1]</span>
<span class="go">[       0       x0        0        0       x0 -x1 - x2]</span>
</pre></div>
</div>
<p>This matrix is normalized so that all columns add to 0. So we need to
add <span class="math notranslate nohighlight">\((x_0 + x_1 + x_2)\)</span> times the <span class="math notranslate nohighlight">\(6\times 6\)</span> identity matrix to get the
probability matrix:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">x</span> <span class="o">=</span> <span class="n">M</span><span class="o">.</span><span class="n">base_ring</span><span class="p">()</span><span class="o">.</span><span class="n">gens</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">Mt</span> <span class="o">=</span> <span class="p">(</span><span class="n">x</span><span class="p">[</span><span class="mi">0</span><span class="p">]</span><span class="o">+</span><span class="n">x</span><span class="p">[</span><span class="mi">1</span><span class="p">]</span><span class="o">+</span><span class="n">x</span><span class="p">[</span><span class="mi">2</span><span class="p">])</span><span class="o">*</span><span class="n">matrix</span><span class="o">.</span><span class="n">identity</span><span class="p">(</span><span class="mi">6</span><span class="p">)</span><span class="o">+</span><span class="n">M</span>
<span class="gp">sage: </span><span class="n">Mt</span>
<span class="go">[x2 x2  0  0 x2  0]</span>
<span class="go">[x1 x1 x1  0  0  0]</span>
<span class="go">[ 0  0 x2 x2  0 x2]</span>
<span class="go">[x0  0 x0 x0  0  0]</span>
<span class="go">[ 0  0  0 x1 x1 x1]</span>
<span class="go">[ 0 x0  0  0 x0 x0]</span>
</pre></div>
</div>
<p>Since the <span class="math notranslate nohighlight">\(x_i\)</span> are formal variables, we need to compute the eigenvalues and
eigenvectors in the symbolic ring <code class="docutils literal notranslate"><span class="pre">SR</span></code>:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">Mt</span><span class="o">.</span><span class="n">change_ring</span><span class="p">(</span><span class="n">SR</span><span class="p">)</span><span class="o">.</span><span class="n">eigenvalues</span><span class="p">()</span>
<span class="go">[x2, x1, x0, x0 + x1 + x2, 0, 0]</span>
</pre></div>
</div>
<p>Do you see any pattern? In fact, if you start playing with bigger values of <span class="math notranslate nohighlight">\(n\)</span> (the size
of the underlying permutations), you might observe that there is an eigenvalue for
every subset <span class="math notranslate nohighlight">\(S\)</span> of <span class="math notranslate nohighlight">\(\{1,2,\ldots,n\}\)</span> and the multiplicity is given by a derangement
number <span class="math notranslate nohighlight">\(d_{n-|S|}\)</span>. Derangment numbers count permutations without fixed point.
For the eigenvectors we obtain:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">Mt</span><span class="o">.</span><span class="n">change_ring</span><span class="p">(</span><span class="n">SR</span><span class="p">)</span><span class="o">.</span><span class="n">eigenvectors_right</span><span class="p">()</span>
<span class="go">[(x2, [(1, 0, -1, 0, 0, 0)], 1),</span>
<span class="go"> (x1, [(0, 1, 0, 0, -1, 0)], 1),</span>
<span class="go"> (x0, [(0, 0, 0, 1, 0, -1)], 1),</span>
<span class="go"> (x0 + x1 + x2,</span>
<span class="go">  [(1,</span>
<span class="go">    (x0 + x1)/(x0 + x2),</span>
<span class="go">    x0/x1,</span>
<span class="go">    (x0^2 + x0*x1)/(x1^2 + x1*x2),</span>
<span class="go">    (x0^2 + x0*x1)/(x0*x2 + x2^2),</span>
<span class="go">    (x0^2 + x0*x1)/(x1*x2 + x2^2))], 1),</span>
<span class="go"> (0, [(1, 0, -1, 0, -1, 1), (0, 1, -1, 1, -1, 0)], 2)]</span>
</pre></div>
</div>
<p>The stationary distribution is the eigenvector of eigenvalues <span class="math notranslate nohighlight">\(1=x_0+x_1+x_2\)</span>. Do you see a pattern?</p>
<div class="topic">
<p class="topic-title">Optional exercices: Study of the transition operators and graph</p>
<p>Instead of using the methods that are already in Sage, try to build the
state space <span class="math notranslate nohighlight">\(\Omega_n\)</span> and the transition operators <span class="math notranslate nohighlight">\(\partial_i\)</span> yourself as follows.</p>
<ol class="arabic">
<li><p>For technical reasons, it is most practical in Sage to label the <span class="math notranslate nohighlight">\(n\)</span> books in the library by
<span class="math notranslate nohighlight">\(0,1,\cdots,n-1\)</span>, and to represent each state in the Markov chain by a permutation
of the set <span class="math notranslate nohighlight">\(\{0,\dots,n-1\}\)</span> as a tuple. Construct the state space <span class="math notranslate nohighlight">\(\Omega_n\)</span> as:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="nb">list</span><span class="p">(</span><span class="nb">map</span><span class="p">(</span><span class="nb">tuple</span><span class="p">,</span> <span class="n">Permutations</span><span class="p">(</span><span class="nb">range</span><span class="p">(</span><span class="mi">3</span><span class="p">))))</span>
<span class="go">[(0, 1, 2), (0, 2, 1), (1, 0, 2), (1, 2, 0), (2, 0, 1), (2, 1, 0)]</span>
</pre></div>
</div>
</li>
<li><p>Write a function <code class="docutils literal notranslate"><span class="pre">transition_operator(sigma,</span> <span class="pre">i)</span></code> which implements the operator
<span class="math notranslate nohighlight">\(\partial_i\)</span> which takes as input a tuple <code class="docutils literal notranslate"><span class="pre">sigma</span></code> and integer <span class="math notranslate nohighlight">\(i \in \{1,2,\ldots,n\}\)</span>
and outputs a new tuple. It might be useful to extract subtuples
(<code class="docutils literal notranslate"><span class="pre">sigma[i:j]</span></code>) and concatenation.</p></li>
<li><p>Write a function <code class="docutils literal notranslate"><span class="pre">tsetlin_digraph(n)</span></code> which constructs the
(multi digraph) as described as shown above. This can be achieved using
<a class="reference external" href="../../reference/graphs/sage/graphs/digraph.html#sage.graphs.digraph.DiGraph" title="(in Graph Theory v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">DiGraph</span></code></a>.</p></li>
<li><p>Verify for which values of <span class="math notranslate nohighlight">\(n\)</span> the digraph is strongly connected
(i.e., you can go from any vertex to any other vertex by going in the direction of the
arrow). This indicates whether the Markov chain is irreducible.</p></li>
</ol>
</div>
</section>
<section id="conclusion">
<h2>Conclusion<a class="headerlink" href="#conclusion" title="Permalink to this headline">¶</a></h2>
<p>The Tsetlin library was studied from the viewpoint of monoids in <a class="reference internal" href="#bidigare1997" id="id2"><span>[Bidigare1997]</span></a>
and <a class="reference internal" href="#brown2000" id="id3"><span>[Brown2000]</span></a>. Precise statements of the eigenvalues and the stationary distribution
of the probability matrix as well as proofs of the statements are given in these papers.
Generalizations of the Tsetlin library from the antichain to
arbitrary posets was given in <a class="reference internal" href="#aks2013" id="id4"><span>[AKS2013]</span></a>.</p>
<dl class="citation">
<dt class="label" id="bidigare1997"><span class="brackets"><a class="fn-backref" href="#id2">Bidigare1997</a></span></dt>
<dd><p>Thomas Patrick Bidigare. <em>Hyperplane arrangement
face algebras and their associated Markov chains</em>. ProQuest LLC,
Ann Arbor, MI, 1997.  Thesis (Ph.D.) University of Michigan.</p>
</dd>
<dt class="label" id="brown2000"><span class="brackets"><a class="fn-backref" href="#id3">Brown2000</a></span></dt>
<dd><p>Kenneth S. Brown. <em>Semigroups, rings, and Markov
chains</em>. J. Theoret.  Probab., 13(3):871-938, 2000.</p>
</dd>
<dt class="label" id="aks2013"><span class="brackets"><a class="fn-backref" href="#id4">AKS2013</a></span></dt>
<dd><p>Arvind Ayyer, Steven Klee, Anne Schilling.
<em>Combinatorial Markov chains on linear extensions</em>
J. Algebraic Combinatorics,
<a class="reference external" href="https://doi.org/10.1007/s10801-013-0470-9">doi:10.1007/s10801-013-0470-9</a>, <a class="reference external" href="https://arxiv.org/abs/1205.7074">arXiv 1205.7074</a>.</p>
</dd>
</dl>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="../index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">The Tsetlin library</a><ul>
<li><a class="reference internal" href="#introduction">Introduction</a></li>
<li><a class="reference internal" href="#id1">The Tsetlin library</a></li>
<li><a class="reference internal" href="#transition-graph-and-matrix">Transition graph and matrix</a></li>
<li><a class="reference internal" href="#conclusion">Conclusion</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="n_cube.html"
                          title="previous chapter"><span class="math notranslate nohighlight">\(n\)</span>-Cube</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="rsk.html"
                          title="next chapter">Young’s lattice and the RSK algorithm</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/algebraic_combinatorics/tsetlin_library.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="rsk.html" title="Young’s lattice and the RSK algorithm"
             >next</a> |</li>
        <li class="right" >
          <a href="n_cube.html" title="\(n\)-Cube"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../index.html">Thematic Tutorials</a> &#187;
    
  </li>

          <li class="nav-item nav-item-1"><a href="../toctree.html" >Thematic tutorial document tree</a> &#187;</li>
          <li class="nav-item nav-item-2"><a href="../algebraic_combinatorics.html" >Algebraic Combinatorics in Sage</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">The Tsetlin library</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>