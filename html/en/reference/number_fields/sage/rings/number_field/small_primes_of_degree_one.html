
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Small primes of degree one &#8212; Algebraic Numbers and Number Fields</title>
    <link rel="stylesheet" type="text/css" href="../../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/thebelab.css" />
    
    <script data-url_root="../../../" id="documentation_options" src="../../../../_static/documentation_options.js"></script>
    <script src="../../../../_static/jquery.js"></script>
    <script src="../../../../_static/underscore.js"></script>
    <script src="../../../../_static/doctools.js"></script>
    <script src="../../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../../genindex.html" />
    <link rel="search" title="Search" href="../../../../search.html" />
    <link rel="next" title="\(p\)-Selmer groups of number fields" href="selmer_group.html" />
    <link rel="prev" title="Solve S-unit equation x + y = 1" href="S_unit_solver.html" />
  <link rel="icon" href="../../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="selmer_group.html" title="\(p\)-Selmer groups of number fields"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="S_unit_solver.html" title="Solve S-unit equation x + y = 1"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Algebraic Numbers and Number Fields</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Small primes of degree one</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="small-primes-of-degree-one">
<span id="sage-rings-number-field-small-primes-of-degree-one"></span><h1>Small primes of degree one<a class="headerlink" href="#small-primes-of-degree-one" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.rings.number_field.small_primes_of_degree_one"></span><p>Iterator for finding several primes of absolute degree one of a number field of
<em>small</em> prime norm.</p>
<hr class="docutils" />
<p><strong>Algorithm</strong>:</p>
<p>Let <span class="math notranslate nohighlight">\(P\)</span> denote the product of some set of prime numbers. (In practice, we
use the product of the first 10000 primes, because Pari computes this many by
default.)</p>
<p>Let <span class="math notranslate nohighlight">\(K\)</span> be a number field and let <span class="math notranslate nohighlight">\(f(x)\)</span> be a polynomial defining <span class="math notranslate nohighlight">\(K\)</span> over the
rational field.  Let <span class="math notranslate nohighlight">\(\alpha\)</span> be a root of <span class="math notranslate nohighlight">\(f\)</span> in <span class="math notranslate nohighlight">\(K\)</span>.</p>
<p>We know that <span class="math notranslate nohighlight">\([ O_K : \ZZ[\alpha] ]^2 = | \Delta(f(x)) / \Delta(O_K) |\)</span>, where
<span class="math notranslate nohighlight">\(\Delta\)</span> denotes the discriminant (see, for example, Proposition 4.4.4, p165 of
<a class="reference internal" href="../../../../references/index.html#coh1993" id="id1"><span>[Coh1993]</span></a>).  Therefore, after discarding primes dividing <span class="math notranslate nohighlight">\(\Delta(f(x))\)</span> (this
includes all ramified primes), any integer <span class="math notranslate nohighlight">\(n\)</span> such that <span class="math notranslate nohighlight">\(\gcd(f(n), P) &gt; 0\)</span>
yields a prime <span class="math notranslate nohighlight">\(p | P\)</span> such that <span class="math notranslate nohighlight">\(f(x)\)</span> has a root modulo <span class="math notranslate nohighlight">\(p\)</span>.  By the
condition on discriminants, this root is a single root.  As is well known (see,
for example Theorem 4.8.13, p199 of <a class="reference internal" href="../../../../references/index.html#coh1993" id="id2"><span>[Coh1993]</span></a>), the ideal generated by <span class="math notranslate nohighlight">\((p, \alpha -
n)\)</span> is prime and of degree one.</p>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<p>It is possible that there are no primes of <span class="math notranslate nohighlight">\(K\)</span> of absolute degree one of
small prime norm, and it is possible that this algorithm will not find
any primes of small norm.</p>
</div>
<hr class="docutils" />
<p><strong>To do</strong>:</p>
<p>There are situations when this will fail.  There are questions of finding
primes of relative degree one.  There are questions of finding primes of exact
degree larger than one.  In short, if you can contribute, please do!</p>
<hr class="docutils" />
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">x</span> <span class="o">=</span> <span class="n">ZZ</span><span class="p">[</span><span class="s1">&#39;x&#39;</span><span class="p">]</span><span class="o">.</span><span class="n">gen</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">F</span><span class="o">.&lt;</span><span class="n">a</span><span class="o">&gt;</span> <span class="o">=</span> <span class="n">NumberField</span><span class="p">(</span><span class="n">x</span><span class="o">^</span><span class="mi">2</span> <span class="o">-</span> <span class="mi">2</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="o">=</span> <span class="n">F</span><span class="o">.</span><span class="n">primes_of_degree_one_list</span><span class="p">(</span><span class="mi">3</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="c1"># random</span>
<span class="go">[Fractional ideal (2*a + 1), Fractional ideal (-3*a + 1), Fractional ideal (-a + 5)]</span>
<span class="gp">sage: </span><span class="p">[</span> <span class="n">P</span><span class="o">.</span><span class="n">norm</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span> <span class="p">]</span> <span class="c1"># random</span>
<span class="go">[7, 17, 23]</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">ZZ</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">norm</span><span class="p">())</span><span class="o">.</span><span class="n">is_prime</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">residue_class_degree</span><span class="p">()</span> <span class="o">==</span> <span class="mi">1</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
</pre></div>
</div>
<p>The next two examples are for relative number fields.:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">L</span><span class="o">.&lt;</span><span class="n">b</span><span class="o">&gt;</span> <span class="o">=</span> <span class="n">F</span><span class="o">.</span><span class="n">extension</span><span class="p">(</span><span class="n">x</span><span class="o">^</span><span class="mi">3</span> <span class="o">-</span> <span class="n">a</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="o">=</span> <span class="n">L</span><span class="o">.</span><span class="n">primes_of_degree_one_list</span><span class="p">(</span><span class="mi">3</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="c1"># random</span>
<span class="go">[Fractional ideal (17, b - 5), Fractional ideal (23, b - 4), Fractional ideal (31, b - 2)]</span>
<span class="gp">sage: </span><span class="p">[</span> <span class="n">P</span><span class="o">.</span><span class="n">absolute_norm</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span> <span class="p">]</span> <span class="c1"># random</span>
<span class="go">[17, 23, 31]</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">ZZ</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">absolute_norm</span><span class="p">())</span><span class="o">.</span><span class="n">is_prime</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">residue_class_degree</span><span class="p">()</span> <span class="o">==</span> <span class="mi">1</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
<span class="gp">sage: </span><span class="n">M</span><span class="o">.&lt;</span><span class="n">c</span><span class="o">&gt;</span> <span class="o">=</span> <span class="n">NumberField</span><span class="p">(</span><span class="n">x</span><span class="o">^</span><span class="mi">2</span> <span class="o">-</span> <span class="n">x</span><span class="o">*</span><span class="n">b</span><span class="o">^</span><span class="mi">2</span> <span class="o">+</span> <span class="n">b</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="o">=</span> <span class="n">M</span><span class="o">.</span><span class="n">primes_of_degree_one_list</span><span class="p">(</span><span class="mi">3</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Ps</span> <span class="c1"># random</span>
<span class="go">[Fractional ideal (17, c - 2), Fractional ideal (c - 1), Fractional ideal (41, c + 15)]</span>
<span class="gp">sage: </span><span class="p">[</span> <span class="n">P</span><span class="o">.</span><span class="n">absolute_norm</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span> <span class="p">]</span> <span class="c1"># random</span>
<span class="go">[17, 31, 41]</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">ZZ</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">absolute_norm</span><span class="p">())</span><span class="o">.</span><span class="n">is_prime</span><span class="p">()</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
<span class="gp">sage: </span><span class="nb">all</span><span class="p">(</span><span class="n">P</span><span class="o">.</span><span class="n">residue_class_degree</span><span class="p">()</span> <span class="o">==</span> <span class="mi">1</span> <span class="k">for</span> <span class="n">P</span> <span class="ow">in</span> <span class="n">Ps</span><span class="p">)</span>
<span class="go">True</span>
</pre></div>
</div>
<p>AUTHORS:</p>
<ul class="simple">
<li><p>Nick Alexander (2008)</p></li>
<li><p>David Loeffler (2009): fixed a bug with relative fields</p></li>
<li><p>Maarten Derickx (2017): fixed a bug with number fields not generated by an integral element</p></li>
</ul>
<dl class="py class">
<dt class="sig sig-object py" id="sage.rings.number_field.small_primes_of_degree_one.Small_primes_of_degree_one_iter">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.rings.number_field.small_primes_of_degree_one.</span></span><span class="sig-name descname"><span class="pre">Small_primes_of_degree_one_iter</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">field</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">num_integer_primes</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">10000</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_iterations</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">100</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.rings.number_field.small_primes_of_degree_one.Small_primes_of_degree_one_iter" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">object</span></code></p>
<p>Iterator that finds primes of a number field of absolute degree
one and bounded small prime norm.</p>
<p>INPUT:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">field</span></code> – a <code class="docutils literal notranslate"><span class="pre">NumberField</span></code>.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">num_integer_primes</span></code> (default: 10000) – an integer.  We try to find
primes of absolute norm no greater than the
<code class="docutils literal notranslate"><span class="pre">num_integer_primes</span></code>-th prime number. For example, if
<code class="docutils literal notranslate"><span class="pre">num_integer_primes</span></code> is 2, the largest norm found will be 3, since
the second prime is 3.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">max_iterations</span></code> (default: 100) – an integer. We test
<code class="docutils literal notranslate"><span class="pre">max_iterations</span></code> integers to find small primes before raising
<code class="docutils literal notranslate"><span class="pre">StopIteration</span></code>.</p></li>
</ul>
<p>AUTHOR:</p>
<ul class="simple">
<li><p>Nick Alexander</p></li>
</ul>
<dl class="py method">
<dt class="sig sig-object py" id="sage.rings.number_field.small_primes_of_degree_one.Small_primes_of_degree_one_iter.next">
<span class="sig-name descname"><span class="pre">next</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#sage.rings.number_field.small_primes_of_degree_one.Small_primes_of_degree_one_iter.next" title="Permalink to this definition">¶</a></dt>
<dd><p>Return a prime of absolute degree one of small prime norm.</p>
<p>Raises <code class="docutils literal notranslate"><span class="pre">StopIteration</span></code> if such a prime cannot be easily found.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">x</span> <span class="o">=</span> <span class="n">QQ</span><span class="p">[</span><span class="s1">&#39;x&#39;</span><span class="p">]</span><span class="o">.</span><span class="n">gen</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">K</span><span class="o">.&lt;</span><span class="n">a</span><span class="o">&gt;</span> <span class="o">=</span> <span class="n">NumberField</span><span class="p">(</span><span class="n">x</span><span class="o">^</span><span class="mi">2</span> <span class="o">-</span> <span class="mi">3</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">it</span> <span class="o">=</span> <span class="n">K</span><span class="o">.</span><span class="n">primes_of_degree_one_iter</span><span class="p">()</span>
<span class="gp">sage: </span><span class="p">[</span> <span class="nb">next</span><span class="p">(</span><span class="n">it</span><span class="p">)</span> <span class="k">for</span> <span class="n">i</span> <span class="ow">in</span> <span class="nb">range</span><span class="p">(</span><span class="mi">3</span><span class="p">)</span> <span class="p">]</span> <span class="c1"># random</span>
<span class="go">[Fractional ideal (2*a + 1), Fractional ideal (-a + 4), Fractional ideal (3*a + 2)]</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="S_unit_solver.html"
                          title="previous chapter">Solve S-unit equation x + y = 1</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="selmer_group.html"
                          title="next chapter"><span class="math notranslate nohighlight">\(p\)</span>-Selmer groups of number fields</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../../_sources/sage/rings/number_field/small_primes_of_degree_one.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="selmer_group.html" title="\(p\)-Selmer groups of number fields"
             >next</a> |</li>
        <li class="right" >
          <a href="S_unit_solver.html" title="Solve S-unit equation x + y = 1"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Algebraic Numbers and Number Fields</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Small primes of degree one</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>