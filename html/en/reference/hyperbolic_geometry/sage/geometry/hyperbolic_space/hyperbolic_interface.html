
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Interface to Hyperbolic Models &#8212; Hyperbolic Geometry</title>
    <link rel="stylesheet" type="text/css" href="../../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/thebelab.css" />
    
    <script data-url_root="../../../" id="documentation_options" src="../../../../_static/documentation_options.js"></script>
    <script src="../../../../_static/jquery.js"></script>
    <script src="../../../../_static/underscore.js"></script>
    <script src="../../../../_static/doctools.js"></script>
    <script src="../../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../../genindex.html" />
    <link rel="search" title="Search" href="../../../../search.html" />
    <link rel="prev" title="Hyperbolic Models" href="hyperbolic_model.html" />
  <link rel="icon" href="../../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="hyperbolic_model.html" title="Hyperbolic Models"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Hyperbolic Geometry</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Interface to Hyperbolic Models</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="interface-to-hyperbolic-models">
<span id="sage-geometry-hyperbolic-space-hyperbolic-interface"></span><h1>Interface to Hyperbolic Models<a class="headerlink" href="#interface-to-hyperbolic-models" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.geometry.hyperbolic_space.hyperbolic_interface"></span><p>This module provides a convenient interface for interacting with models
of hyperbolic space as well as their points, geodesics, and isometries.</p>
<p>The primary point of this module is to allow the code that implements
hyperbolic space to be sufficiently decoupled while still providing a
convenient user experience.</p>
<p>The interfaces are by default given abbreviated names.  For example,
UHP (upper half plane model), PD (Poincaré disk model), KM (Klein disk
model), and HM (hyperboloid model).</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>All of the current models of 2 dimensional hyperbolic space
use the upper half plane model for their computations.  This can
lead to some problems, such as long coordinate strings for symbolic
points.  For example, the vector <code class="docutils literal notranslate"><span class="pre">(1,</span> <span class="pre">0,</span> <span class="pre">sqrt(2))</span></code> defines a point
in the hyperboloid model.  Performing mapping this point to the upper
half plane and performing computations there may return with vector
whose components are unsimplified strings have several <code class="docutils literal notranslate"><span class="pre">sqrt(2)</span></code>’s.
Presently, this drawback is outweighted by the rapidity with which new
models can be implemented.</p>
</div>
<p>AUTHORS:</p>
<ul class="simple">
<li><p>Greg Laun (2013): Initial version.</p></li>
<li><p>Rania Amer, Jean-Philippe Burelle, Bill Goldman, Zach Groton,
Jeremy Lent, Leila Vaden, Derrick Wigglesworth (2011): many of the
methods spread across the files.</p></li>
</ul>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">HyperbolicPlane</span><span class="p">()</span><span class="o">.</span><span class="n">UHP</span><span class="p">()</span><span class="o">.</span><span class="n">get_point</span><span class="p">(</span><span class="mi">2</span> <span class="o">+</span> <span class="n">I</span><span class="p">)</span>
<span class="go">Point in UHP I + 2</span>

<span class="gp">sage: </span><span class="n">HyperbolicPlane</span><span class="p">()</span><span class="o">.</span><span class="n">PD</span><span class="p">()</span><span class="o">.</span><span class="n">get_point</span><span class="p">(</span><span class="mi">1</span><span class="o">/</span><span class="mi">2</span> <span class="o">+</span> <span class="n">I</span><span class="o">/</span><span class="mi">2</span><span class="p">)</span>
<span class="go">Point in PD 1/2*I + 1/2</span>
</pre></div>
</div>
<dl class="py class">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_interface.</span></span><span class="sig-name descname"><span class="pre">HyperbolicModels</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">base</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference external" href="../../../../categories/sage/categories/realizations.html#sage.categories.realizations.Category_realization_of_parent" title="(in Category Framework v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.categories.realizations.Category_realization_of_parent</span></code></a></p>
<p>The category of hyperbolic models of hyperbolic space.</p>
<dl class="py class">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels.ParentMethods">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-name descname"><span class="pre">ParentMethods</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels.ParentMethods" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">object</span></code></p>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels.super_categories">
<span class="sig-name descname"><span class="pre">super_categories</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicModels.super_categories" title="Permalink to this definition">¶</a></dt>
<dd><p>The super categories of <code class="docutils literal notranslate"><span class="pre">self</span></code>.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.geometry.hyperbolic_space.hyperbolic_interface</span> <span class="kn">import</span> <span class="n">HyperbolicModels</span>
<span class="gp">sage: </span><span class="n">H</span> <span class="o">=</span> <span class="n">HyperbolicPlane</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">models</span> <span class="o">=</span> <span class="n">HyperbolicModels</span><span class="p">(</span><span class="n">H</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">models</span><span class="o">.</span><span class="n">super_categories</span><span class="p">()</span>
<span class="go">[Category of metric spaces,</span>
<span class="go"> Category of realizations of Hyperbolic plane]</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

<dl class="py class">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_interface.</span></span><span class="sig-name descname"><span class="pre">HyperbolicPlane</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference external" href="../../../../structure/sage/structure/parent.html#sage.structure.parent.Parent" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.parent.Parent</span></code></a>, <a class="reference external" href="../../../../structure/sage/structure/unique_representation.html#sage.structure.unique_representation.UniqueRepresentation" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.unique_representation.UniqueRepresentation</span></code></a></p>
<p>The hyperbolic plane <span class="math notranslate nohighlight">\(\mathbb{H}^2\)</span>.</p>
<p>Here are the models currently implemented:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">UHP</span></code> – upper half plane</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">PD</span></code> – Poincaré disk</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">KM</span></code> – Klein disk</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">HM</span></code> – hyperboloid model</p></li>
</ul>
<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.HM">
<span class="sig-name descname"><span class="pre">HM</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.HM" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.Hyperboloid">
<span class="sig-name descname"><span class="pre">Hyperboloid</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.Hyperboloid" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelHM</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.KM">
<span class="sig-name descname"><span class="pre">KM</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.KM" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.KleinDisk">
<span class="sig-name descname"><span class="pre">KleinDisk</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.KleinDisk" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelKM</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.PD">
<span class="sig-name descname"><span class="pre">PD</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.PD" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.PoincareDisk">
<span class="sig-name descname"><span class="pre">PoincareDisk</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.PoincareDisk" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelPD</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.UHP">
<span class="sig-name descname"><span class="pre">UHP</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.UHP" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP</span></code></a></p>
</dd></dl>

<dl class="py attribute">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.UpperHalfPlane">
<span class="sig-name descname"><span class="pre">UpperHalfPlane</span></span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.UpperHalfPlane" title="Permalink to this definition">¶</a></dt>
<dd><p>alias of <a class="reference internal" href="hyperbolic_model.html#sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP" title="sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_model.HyperbolicModelUHP</span></code></a></p>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.a_realization">
<span class="sig-name descname"><span class="pre">a_realization</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicPlane.a_realization" title="Permalink to this definition">¶</a></dt>
<dd><p>Return a realization of <code class="docutils literal notranslate"><span class="pre">self</span></code>.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">H</span> <span class="o">=</span> <span class="n">HyperbolicPlane</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">H</span><span class="o">.</span><span class="n">a_realization</span><span class="p">()</span>
<span class="go">Hyperbolic plane in the Upper Half Plane Model</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

<dl class="py function">
<dt class="sig sig-object py" id="sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicSpace">
<span class="sig-prename descclassname"><span class="pre">sage.geometry.hyperbolic_space.hyperbolic_interface.</span></span><span class="sig-name descname"><span class="pre">HyperbolicSpace</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">n</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.geometry.hyperbolic_space.hyperbolic_interface.HyperbolicSpace" title="Permalink to this definition">¶</a></dt>
<dd><p>Return <code class="docutils literal notranslate"><span class="pre">n</span></code> dimensional hyperbolic space.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.geometry.hyperbolic_space.hyperbolic_interface</span> <span class="kn">import</span> <span class="n">HyperbolicSpace</span>
<span class="gp">sage: </span><span class="n">HyperbolicSpace</span><span class="p">(</span><span class="mi">2</span><span class="p">)</span>
<span class="go">Hyperbolic plane</span>
</pre></div>
</div>
</dd></dl>

</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="hyperbolic_model.html"
                          title="previous chapter">Hyperbolic Models</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../../_sources/sage/geometry/hyperbolic_space/hyperbolic_interface.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="hyperbolic_model.html" title="Hyperbolic Models"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Hyperbolic Geometry</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Interface to Hyperbolic Models</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>