
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Bandwidth of undirected graphs &#8212; Graph Theory</title>
    <link rel="stylesheet" type="text/css" href="../../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/thebelab.css" />
    
    <script data-url_root="../../../" id="documentation_options" src="../../../../_static/documentation_options.js"></script>
    <script src="../../../../_static/jquery.js"></script>
    <script src="../../../../_static/underscore.js"></script>
    <script src="../../../../_static/doctools.js"></script>
    <script src="../../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../../genindex.html" />
    <link rel="search" title="Search" href="../../../../search.html" />
    <link rel="next" title="Cutwidth" href="cutwidth.html" />
    <link rel="prev" title="Rank Decompositions of graphs" href="rankwidth.html" />
  <link rel="icon" href="../../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="cutwidth.html" title="Cutwidth"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="rankwidth.html" title="Rank Decompositions of graphs"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Graph Theory</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Bandwidth of undirected graphs</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="bandwidth-of-undirected-graphs">
<span id="sage-graphs-graph-decompositions-bandwidth"></span><h1>Bandwidth of undirected graphs<a class="headerlink" href="#bandwidth-of-undirected-graphs" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.graphs.graph_decompositions.bandwidth"></span><section id="definition">
<h2>Definition<a class="headerlink" href="#definition" title="Permalink to this headline">¶</a></h2>
<p>The bandwidth <span class="math notranslate nohighlight">\(bw(M)\)</span> of a matrix <span class="math notranslate nohighlight">\(M\)</span> is the smallest integer <span class="math notranslate nohighlight">\(k\)</span> such that all
non-zero entries of <span class="math notranslate nohighlight">\(M\)</span> are at distance <span class="math notranslate nohighlight">\(k\)</span> from the diagonal. The bandwidth
<span class="math notranslate nohighlight">\(bw(G)\)</span> of an undirected graph <span class="math notranslate nohighlight">\(G\)</span> is the minimum bandwidth of the adjacency
matrix of <span class="math notranslate nohighlight">\(G\)</span>, over all possible relabellings of its vertices.</p>
<p><strong>Path spanner:</strong> alternatively, the bandwidth measures how tightly a path
represents the distance of a graph <span class="math notranslate nohighlight">\(G\)</span>. Indeed, if the vertices of <span class="math notranslate nohighlight">\(G\)</span> can be
ordered as <span class="math notranslate nohighlight">\(v_1,...,v_n\)</span> in such a way that <span class="math notranslate nohighlight">\(k \times d_G(v_i,v_j) \geq |i-j|\)</span> then
<span class="math notranslate nohighlight">\(bw(G)\leq k\)</span>.</p>
<blockquote>
<div><p><strong>Proof:</strong> for all <span class="math notranslate nohighlight">\(v_i \sim v_j\)</span> (i.e. <span class="math notranslate nohighlight">\(d_G(v_i,v_j)=1\)</span>), the constraint
ensures that <span class="math notranslate nohighlight">\(k\geq |i-j|\)</span>, meaning that adjacent vertices are at distance
at most <span class="math notranslate nohighlight">\(k\)</span> in the path ordering. That alone is sufficient to ensure that
<span class="math notranslate nohighlight">\(bw(G)\leq k\)</span>.</p>
<p>As a byproduct, we obtain that <span class="math notranslate nohighlight">\(k \times d_G(v_i,v_j) \geq |i-j|\)</span> in
general: let <span class="math notranslate nohighlight">\(v_{s_0},...,v_{s_i}\)</span> be the vertices of a shortest
<span class="math notranslate nohighlight">\((v_i,v_j)\)</span>-path. We have:</p>
<div class="math notranslate nohighlight">
\[\begin{split}k \times d_G(v_i,v_j) &amp;=    k\times d_G(v_i,v_{s_0}) + k\times d_G(v_{s_0},v_{s_1}) + ... + k\times d_G(v_{s_{i-1}},v_{s_i}) + k\times d_G(v_{s_i},v_j)\\
                      &amp;\geq |v_i-v_{s_0}| + |v_{s_0}-v_{s_1}| + ... + |v_{s_{i-1}}-v_{s_i}| + |v_{s_i}-v_j|\\
                      &amp;\geq |v_i-v_j|\\\end{split}\]</div>
</div></blockquote>
</section>
<section id="satisfiability-of-a-partial-assignment">
<h2>Satisfiability of a partial assignment<a class="headerlink" href="#satisfiability-of-a-partial-assignment" title="Permalink to this headline">¶</a></h2>
<p>Let us suppose that the first <span class="math notranslate nohighlight">\(i\)</span> vertices <span class="math notranslate nohighlight">\(v_1,...,v_i\)</span> of <span class="math notranslate nohighlight">\(G\)</span> have already
been assigned positions <span class="math notranslate nohighlight">\(p_1,...,p_i\)</span> in an ordering of <span class="math notranslate nohighlight">\(V(G)\)</span> of bandwidth
<span class="math notranslate nohighlight">\(\leq k\)</span>. Where can <span class="math notranslate nohighlight">\(v_{i+1}\)</span> appear ?</p>
<p>Because of the previous definition, <span class="math notranslate nohighlight">\(p_{i+1}\)</span> must be at distance at most
<span class="math notranslate nohighlight">\(k\times d_G(v_1,v_{i+1})\)</span> from <span class="math notranslate nohighlight">\(p_1\)</span>, and in general at distance at most
<span class="math notranslate nohighlight">\(k\times d_G(v_j,v_{i+1})\)</span> from <span class="math notranslate nohighlight">\(p_j\)</span>. Each range is an interval of
<span class="math notranslate nohighlight">\(\{1,...,n\}\backslash \{p_1,...,p_i\}\)</span>, and because the intersection of two
intervals is again an interval we deduce that in order to satisfy all these
constraints simultaneously <span class="math notranslate nohighlight">\(p_j\)</span> must belong to an interval defined from this
partial assignment.</p>
<p>Applying this rule to all non-assigned vertices, we deduce that each of them
must be assigned to a given interval of <span class="math notranslate nohighlight">\(\{1,...,n\}\)</span>. Note that this can also
be extended to the already assigned vertices, by saying that <span class="math notranslate nohighlight">\(v_j\)</span> with <span class="math notranslate nohighlight">\(j&lt;i\)</span>
must be assigned within the interval <span class="math notranslate nohighlight">\([p_j,p_j]\)</span>.</p>
<p>This problem is not always satisfiable, e.g. 5 vertices cannot all be assigned
to the elements of <span class="math notranslate nohighlight">\([10,13]\)</span>. This is a matching problem which, because all
admissible sets are intervals, can be solved quickly.</p>
</section>
<section id="solving-the-matching-problem">
<h2>Solving the matching problem<a class="headerlink" href="#solving-the-matching-problem" title="Permalink to this headline">¶</a></h2>
<p>Let <span class="math notranslate nohighlight">\(n\)</span> points <span class="math notranslate nohighlight">\(v_1,...,v_n\)</span> be given, along with two functions <span class="math notranslate nohighlight">\(m,M:[n]\mapsto
[n]\)</span>. Is there an ordering <span class="math notranslate nohighlight">\(p_1,...,p_n\)</span> of them such that <span class="math notranslate nohighlight">\(m(v_i) \leq p_i \leq
M(v_i)\)</span> ? This is equivalent to Hall’s bipartite matching theorem, and can in
this specific case be solved by the following algorithm:</p>
<ul class="simple">
<li><p>Consider all vertices <span class="math notranslate nohighlight">\(v\)</span> sorted increasingly according to <span class="math notranslate nohighlight">\(M(v)\)</span></p></li>
<li><p>For each of them, assign to <span class="math notranslate nohighlight">\(v\)</span> the smallest position in <span class="math notranslate nohighlight">\([m(v),M(v)]\)</span> which
has not been assigned yet. If there is none, the assignment problem is not
satisfiable.</p></li>
</ul>
<p>Note that the latest operation can be performed with very few bitset operations
(provided that <span class="math notranslate nohighlight">\(n&lt;64\)</span>).</p>
</section>
<section id="the-algorithm">
<h2>The algorithm<a class="headerlink" href="#the-algorithm" title="Permalink to this headline">¶</a></h2>
<p>This section contains totally subjective choices, that may be changed in the
hope to get better performances.</p>
<ul class="simple">
<li><p>Try to find a satisfiable ordering by filling positions, one after the other
(and not by trying to find each vertex’ position)</p></li>
<li><p>Fill the positions in this order: <span class="math notranslate nohighlight">\(0,n-1,1,n-2,3,n-3, ...\)</span></p></li>
</ul>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>There is some symmetry to break as the reverse of a satisfiable ordering is
also a satisfiable ordering.</p>
</div>
</section>
<section id="this-module-contains-the-following-methods">
<h2>This module contains the following methods<a class="headerlink" href="#this-module-contains-the-following-methods" title="Permalink to this headline">¶</a></h2>
<table class="colwidths-given contentstable docutils align-default">
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="row-odd"><td><p><a class="reference internal" href="#sage.graphs.graph_decompositions.bandwidth.bandwidth" title="sage.graphs.graph_decompositions.bandwidth.bandwidth"><code class="xref py py-meth docutils literal notranslate"><span class="pre">bandwidth()</span></code></a></p></td>
<td><p>Compute the bandwidth of an undirected graph</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference internal" href="../base/boost_graph.html#sage.graphs.base.boost_graph.bandwidth_heuristics" title="sage.graphs.base.boost_graph.bandwidth_heuristics"><code class="xref py py-meth docutils literal notranslate"><span class="pre">bandwidth_heuristics()</span></code></a></p></td>
<td><p>Use Boost heuristics to approximate the bandwidth of the input graph</p></td>
</tr>
</tbody>
</table>
</section>
<section id="functions">
<h2>Functions<a class="headerlink" href="#functions" title="Permalink to this headline">¶</a></h2>
<dl class="py function">
<dt class="sig sig-object py" id="sage.graphs.graph_decompositions.bandwidth.bandwidth">
<span class="sig-prename descclassname"><span class="pre">sage.graphs.graph_decompositions.bandwidth.</span></span><span class="sig-name descname"><span class="pre">bandwidth</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">G</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">k</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.graphs.graph_decompositions.bandwidth.bandwidth" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the bandwidth of an undirected graph.</p>
<p>For a definition of the bandwidth of a graph, see the documentation of the
<a class="reference internal" href="#module-sage.graphs.graph_decompositions.bandwidth" title="sage.graphs.graph_decompositions.bandwidth"><code class="xref py py-mod docutils literal notranslate"><span class="pre">bandwidth</span></code></a> module.</p>
<p>INPUT:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">G</span></code> – a graph</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">k</span></code> – integer (default: <code class="docutils literal notranslate"><span class="pre">None</span></code>); set to an integer value to test
whether <span class="math notranslate nohighlight">\(bw(G)\leq k\)</span>, or to <code class="docutils literal notranslate"><span class="pre">None</span></code> (default) to compute <span class="math notranslate nohighlight">\(bw(G)\)</span></p></li>
</ul>
<p>OUTPUT:</p>
<p>When <span class="math notranslate nohighlight">\(k\)</span> is an integer value, the function returns either <code class="docutils literal notranslate"><span class="pre">False</span></code> or an
ordering of cost <span class="math notranslate nohighlight">\(\leq k\)</span>.</p>
<p>When <span class="math notranslate nohighlight">\(k\)</span> is equal to <code class="docutils literal notranslate"><span class="pre">None</span></code>, the function returns a pair <code class="docutils literal notranslate"><span class="pre">(bw,</span>
<span class="pre">ordering)</span></code>.</p>
<div class="admonition seealso">
<p class="admonition-title">See also</p>
<p><a class="reference internal" href="../generic_graph.html#sage.graphs.generic_graph.GenericGraph.adjacency_matrix" title="sage.graphs.generic_graph.GenericGraph.adjacency_matrix"><code class="xref py py-meth docutils literal notranslate"><span class="pre">sage.graphs.generic_graph.GenericGraph.adjacency_matrix()</span></code></a> –
return the adjacency matrix from an ordering of the vertices.</p>
</div>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.graphs.graph_decompositions.bandwidth</span> <span class="kn">import</span> <span class="n">bandwidth</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">PetersenGraph</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">bandwidth</span><span class="p">(</span><span class="n">G</span><span class="p">,</span><span class="mi">3</span><span class="p">)</span>
<span class="go">False</span>
<span class="gp">sage: </span><span class="n">bandwidth</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>
<span class="go">(5, [0, 4, 5, 8, 1, 9, 3, 7, 6, 2])</span>
<span class="gp">sage: </span><span class="n">G</span><span class="o">.</span><span class="n">adjacency_matrix</span><span class="p">(</span><span class="n">vertices</span><span class="o">=</span><span class="p">[</span><span class="mi">0</span><span class="p">,</span> <span class="mi">4</span><span class="p">,</span> <span class="mi">5</span><span class="p">,</span> <span class="mi">8</span><span class="p">,</span> <span class="mi">1</span><span class="p">,</span> <span class="mi">9</span><span class="p">,</span> <span class="mi">3</span><span class="p">,</span> <span class="mi">7</span><span class="p">,</span> <span class="mi">6</span><span class="p">,</span> <span class="mi">2</span><span class="p">])</span>
<span class="go">[0 1 1 0 1 0 0 0 0 0]</span>
<span class="go">[1 0 0 0 0 1 1 0 0 0]</span>
<span class="go">[1 0 0 1 0 0 0 1 0 0]</span>
<span class="go">[0 0 1 0 0 0 1 0 1 0]</span>
<span class="go">[1 0 0 0 0 0 0 0 1 1]</span>
<span class="go">[0 1 0 0 0 0 0 1 1 0]</span>
<span class="go">[0 1 0 1 0 0 0 0 0 1]</span>
<span class="go">[0 0 1 0 0 1 0 0 0 1]</span>
<span class="go">[0 0 0 1 1 1 0 0 0 0]</span>
<span class="go">[0 0 0 0 1 0 1 1 0 0]</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">ChvatalGraph</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">bandwidth</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>
<span class="go">(6, [0, 5, 9, 4, 10, 1, 6, 11, 3, 8, 7, 2])</span>
<span class="gp">sage: </span><span class="n">G</span><span class="o">.</span><span class="n">adjacency_matrix</span><span class="p">(</span><span class="n">vertices</span><span class="o">=</span><span class="p">[</span><span class="mi">0</span><span class="p">,</span> <span class="mi">5</span><span class="p">,</span> <span class="mi">9</span><span class="p">,</span> <span class="mi">4</span><span class="p">,</span> <span class="mi">10</span><span class="p">,</span> <span class="mi">1</span><span class="p">,</span> <span class="mi">6</span><span class="p">,</span> <span class="mi">11</span><span class="p">,</span> <span class="mi">3</span><span class="p">,</span> <span class="mi">8</span><span class="p">,</span> <span class="mi">7</span><span class="p">,</span> <span class="mi">2</span><span class="p">])</span>
<span class="go">[0 0 1 1 0 1 1 0 0 0 0 0]</span>
<span class="go">[0 0 0 1 1 1 0 1 0 0 0 0]</span>
<span class="go">[1 0 0 0 1 0 0 1 1 0 0 0]</span>
<span class="go">[1 1 0 0 0 0 0 0 1 1 0 0]</span>
<span class="go">[0 1 1 0 0 0 1 0 0 1 0 0]</span>
<span class="go">[1 1 0 0 0 0 0 0 0 0 1 1]</span>
<span class="go">[1 0 0 0 1 0 0 1 0 0 0 1]</span>
<span class="go">[0 1 1 0 0 0 1 0 0 0 1 0]</span>
<span class="go">[0 0 1 1 0 0 0 0 0 0 1 1]</span>
<span class="go">[0 0 0 1 1 0 0 0 0 0 1 1]</span>
<span class="go">[0 0 0 0 0 1 0 1 1 1 0 0]</span>
<span class="go">[0 0 0 0 0 1 1 0 1 1 0 0]</span>
</pre></div>
</div>
</dd></dl>

</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="../../../index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Bandwidth of undirected graphs</a><ul>
<li><a class="reference internal" href="#definition">Definition</a></li>
<li><a class="reference internal" href="#satisfiability-of-a-partial-assignment">Satisfiability of a partial assignment</a></li>
<li><a class="reference internal" href="#solving-the-matching-problem">Solving the matching problem</a></li>
<li><a class="reference internal" href="#the-algorithm">The algorithm</a></li>
<li><a class="reference internal" href="#this-module-contains-the-following-methods">This module contains the following methods</a></li>
<li><a class="reference internal" href="#functions">Functions</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="rankwidth.html"
                          title="previous chapter">Rank Decompositions of graphs</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="cutwidth.html"
                          title="next chapter">Cutwidth</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../../_sources/sage/graphs/graph_decompositions/bandwidth.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="cutwidth.html" title="Cutwidth"
             >next</a> |</li>
        <li class="right" >
          <a href="rankwidth.html" title="Rank Decompositions of graphs"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Graph Theory</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Bandwidth of undirected graphs</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>