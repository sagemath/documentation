
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Genus &#8212; Graph Theory</title>
    <link rel="stylesheet" type="text/css" href="../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../_static/thebelab.css" />
    
    <script data-url_root="../../" id="documentation_options" src="../../../_static/documentation_options.js"></script>
    <script src="../../../_static/jquery.js"></script>
    <script src="../../../_static/underscore.js"></script>
    <script src="../../../_static/doctools.js"></script>
    <script src="../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../genindex.html" />
    <link rel="search" title="Search" href="../../../search.html" />
    <link rel="next" title="Lovász theta-function of graphs" href="lovasz_theta.html" />
    <link rel="prev" title="Matching Polynomial" href="matchpoly.html" />
  <link rel="icon" href="../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="lovasz_theta.html" title="Lovász theta-function of graphs"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="matchpoly.html" title="Matching Polynomial"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../index.html">Graph Theory</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Genus</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="genus">
<span id="sage-graphs-genus"></span><h1>Genus<a class="headerlink" href="#genus" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.graphs.genus"></span><p>This file contains a moderately-optimized implementation to compute the
genus of simple connected graph.  It runs about a thousand times faster
than the previous version in Sage, not including asymptotic improvements.</p>
<p>The algorithm works by enumerating combinatorial embeddings of a graph,
and computing the genus of these via the Euler characteristic.  We view
a combinatorial embedding of a graph as a pair of permutations <span class="math notranslate nohighlight">\(v,e\)</span>
which act on a set <span class="math notranslate nohighlight">\(B\)</span> of <span class="math notranslate nohighlight">\(2|E(G)|\)</span> “darts”.  The permutation <span class="math notranslate nohighlight">\(e\)</span> is an
involution, and its orbits correspond to edges in the graph.  Similarly,
The orbits of <span class="math notranslate nohighlight">\(v\)</span> correspond to the vertices of the graph, and those of
<span class="math notranslate nohighlight">\(f = ve\)</span> correspond to faces of the embedded graph.</p>
<p>The requirement that the group <span class="math notranslate nohighlight">\(&lt;v,e&gt;\)</span> acts transitively on <span class="math notranslate nohighlight">\(B\)</span> is
equivalent to the graph being connected.  We can compute the genus of a
graph by</p>
<blockquote>
<div><p><span class="math notranslate nohighlight">\(2 - 2g = V - E + F\)</span></p>
</div></blockquote>
<p>where <span class="math notranslate nohighlight">\(E\)</span>, <span class="math notranslate nohighlight">\(V\)</span>, and <span class="math notranslate nohighlight">\(F\)</span> denote the number of orbits of <span class="math notranslate nohighlight">\(e\)</span>, <span class="math notranslate nohighlight">\(v\)</span>, and
<span class="math notranslate nohighlight">\(f\)</span> respectively.</p>
<p>We make several optimizations to the naive algorithm, which are
described throughout the file.</p>
<dl class="py class">
<dt class="sig sig-object py" id="sage.graphs.genus.simple_connected_genus_backtracker">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.graphs.genus.</span></span><span class="sig-name descname"><span class="pre">simple_connected_genus_backtracker</span></span><a class="headerlink" href="#sage.graphs.genus.simple_connected_genus_backtracker" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">object</span></code></p>
<p>A class which computes the genus of a DenseGraph through an extremely slow
but relatively optimized algorithm.  This is “only” exponential for graphs
of bounded degree, and feels pretty snappy for 3-regular graphs. The
generic runtime is</p>
<blockquote>
<div><p><span class="math notranslate nohighlight">\(|V(G)| \prod_{v \in V(G)} (deg(v)-1)!\)</span></p>
</div></blockquote>
<p>which is <span class="math notranslate nohighlight">\(2^{|V(G)|}\)</span> for 3-regular graphs, and can achieve <span class="math notranslate nohighlight">\(n(n-1)!^{n}\)</span>
for the complete graph on <span class="math notranslate nohighlight">\(n\)</span> vertices.  We can handily compute the genus of
<span class="math notranslate nohighlight">\(K_6\)</span> in milliseconds on modern hardware, but <span class="math notranslate nohighlight">\(K_7\)</span> may take a few days.
Don’t bother with <span class="math notranslate nohighlight">\(K_8\)</span>, or any graph with more than one vertex of degree 10
or worse, unless you can find an a priori lower bound on the genus and
expect the graph to have that genus.</p>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<dl class="simple">
<dt>THIS MAY SEGFAULT OR HANG ON:</dt><dd><ul class="simple">
<li><p>DISCONNECTED GRAPHS</p></li>
<li><p>DIRECTED GRAPHS</p></li>
<li><p>LOOPED GRAPHS</p></li>
<li><p>MULTIGRAPHS</p></li>
</ul>
</dd>
</dl>
</div>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">import</span> <span class="nn">sage.graphs.genus</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">CompleteGraph</span><span class="p">(</span><span class="mi">6</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">G</span><span class="p">,</span> <span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">bt</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">bt</span><span class="o">.</span><span class="n">genus</span><span class="p">()</span> <span class="c1">#long time</span>
<span class="go">1</span>
<span class="gp">sage: </span><span class="n">bt</span><span class="o">.</span><span class="n">genus</span><span class="p">(</span><span class="n">cutoff</span><span class="o">=</span><span class="mi">1</span><span class="p">)</span>
<span class="go">1</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">PetersenGraph</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">G</span><span class="p">,</span> <span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">bt</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">bt</span><span class="o">.</span><span class="n">genus</span><span class="p">()</span>
<span class="go">1</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">FlowerSnark</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">G</span><span class="p">,</span> <span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">bt</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">bt</span><span class="o">.</span><span class="n">genus</span><span class="p">()</span>
<span class="go">2</span>
</pre></div>
</div>
<dl class="py method">
<dt class="sig sig-object py" id="sage.graphs.genus.simple_connected_genus_backtracker.genus">
<span class="sig-name descname"><span class="pre">genus</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">style</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">1</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">cutoff</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">0</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">record_embedding</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">False</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.graphs.genus.simple_connected_genus_backtracker.genus" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the minimal or maximal genus of self’s graph.</p>
<p>Note, this is a remarkably naive algorithm for a very difficult problem.
Most interesting cases will take millennia to finish, with the exception
of graphs with max degree 3.</p>
<p>INPUT:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">style</span></code> – integer (default: <code class="docutils literal notranslate"><span class="pre">1</span></code>); find minimum genus if 1,
maximum genus if 2</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">cutoff</span></code> – integer (default: <code class="docutils literal notranslate"><span class="pre">0</span></code>); stop searching if search style
is 1 and <code class="docutils literal notranslate"><span class="pre">genus</span></code> <span class="math notranslate nohighlight">\(\leq\)</span> <code class="docutils literal notranslate"><span class="pre">cutoff</span></code>, or if style is 2 and <code class="docutils literal notranslate"><span class="pre">genus</span></code>
<span class="math notranslate nohighlight">\(\geq\)</span> <code class="docutils literal notranslate"><span class="pre">cutoff</span></code>.  This is useful where the genus of the graph has a
known bound.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">record_embedding</span></code> – boolean (default: <code class="docutils literal notranslate"><span class="pre">False</span></code>); whether or not
to remember the best embedding seen. This embedding can be retrieved
with <code class="docutils literal notranslate"><span class="pre">self.get_embedding()</span></code>.</p></li>
</ul>
<p>OUTPUT:</p>
<blockquote>
<div><p>the minimal or maximal genus for self’s graph.</p>
</div></blockquote>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">import</span> <span class="nn">sage.graphs.genus</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">graphs</span><span class="o">.</span><span class="n">CompleteGraph</span><span class="p">(</span><span class="mi">5</span><span class="p">),</span> <span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">gb</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">genus</span><span class="p">(</span><span class="n">cutoff</span><span class="o">=</span><span class="mi">2</span><span class="p">,</span> <span class="n">record_embedding</span><span class="o">=</span><span class="kc">True</span><span class="p">)</span>
<span class="go">2</span>
<span class="gp">sage: </span><span class="n">E</span> <span class="o">=</span> <span class="n">gb</span><span class="o">.</span><span class="n">get_embedding</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">genus</span><span class="p">(</span><span class="n">record_embedding</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="go">1</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">get_embedding</span><span class="p">()</span> <span class="o">==</span> <span class="n">E</span>
<span class="go">True</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">genus</span><span class="p">(</span><span class="n">style</span><span class="o">=</span><span class="mi">2</span><span class="p">,</span> <span class="n">cutoff</span><span class="o">=</span><span class="mi">5</span><span class="p">)</span>
<span class="go">3</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">gb</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">genus</span><span class="p">()</span>
<span class="go">0</span>
</pre></div>
</div>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="sage.graphs.genus.simple_connected_genus_backtracker.get_embedding">
<span class="sig-name descname"><span class="pre">get_embedding</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#sage.graphs.genus.simple_connected_genus_backtracker.get_embedding" title="Permalink to this definition">¶</a></dt>
<dd><p>Return an embedding for the graph.</p>
<p>If <code class="docutils literal notranslate"><span class="pre">min_genus_backtrack</span></code> has been called with <code class="docutils literal notranslate"><span class="pre">record_embedding</span> <span class="pre">=</span>
<span class="pre">True</span></code>, then this will return the first minimal embedding that we found.
Otherwise, this returns the first embedding considered.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">import</span> <span class="nn">sage.graphs.genus</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">graphs</span><span class="o">.</span><span class="n">CompleteGraph</span><span class="p">(</span><span class="mi">5</span><span class="p">),</span> <span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">gb</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">genus</span><span class="p">(</span><span class="n">record_embedding</span><span class="o">=</span><span class="kc">True</span><span class="p">)</span>
<span class="go">1</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">get_embedding</span><span class="p">()</span>
<span class="go">{0: [1, 2, 3, 4], 1: [0, 2, 3, 4], 2: [0, 1, 4, 3], 3: [0, 2, 1, 4], 4: [0, 3, 1, 2]}</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">G</span><span class="o">.</span><span class="n">add_edge</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">gb</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">get_embedding</span><span class="p">()</span>
<span class="go">{0: [1], 1: [0]}</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">Graph</span><span class="p">(</span><span class="n">sparse</span><span class="o">=</span><span class="kc">False</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">gb</span> <span class="o">=</span> <span class="n">sage</span><span class="o">.</span><span class="n">graphs</span><span class="o">.</span><span class="n">genus</span><span class="o">.</span><span class="n">simple_connected_genus_backtracker</span><span class="p">(</span><span class="n">G</span><span class="o">.</span><span class="n">_backend</span><span class="o">.</span><span class="n">c_graph</span><span class="p">()[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">gb</span><span class="o">.</span><span class="n">get_embedding</span><span class="p">()</span>
<span class="go">{}</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

<dl class="py function">
<dt class="sig sig-object py" id="sage.graphs.genus.simple_connected_graph_genus">
<span class="sig-prename descclassname"><span class="pre">sage.graphs.genus.</span></span><span class="sig-name descname"><span class="pre">simple_connected_graph_genus</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">G</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">set_embedding</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">False</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">check</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">True</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">minimal</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">True</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.graphs.genus.simple_connected_graph_genus" title="Permalink to this definition">¶</a></dt>
<dd><p>Compute the genus of a simple connected graph.</p>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<dl class="simple">
<dt>THIS MAY SEGFAULT OR HANG ON:</dt><dd><ul class="simple">
<li><p>DISCONNECTED GRAPHS</p></li>
<li><p>DIRECTED GRAPHS</p></li>
<li><p>LOOPED GRAPHS</p></li>
<li><p>MULTIGRAPHS</p></li>
</ul>
</dd>
</dl>
<p>DO NOT CALL WITH <code class="docutils literal notranslate"><span class="pre">check</span> <span class="pre">=</span> <span class="pre">False</span></code> UNLESS YOU ARE CERTAIN.</p>
</div>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">import</span> <span class="nn">sage.graphs.genus</span>
<span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.graphs.genus</span> <span class="kn">import</span> <span class="n">simple_connected_graph_genus</span> <span class="k">as</span> <span class="n">genus</span>
<span class="gp">sage: </span><span class="p">[</span><span class="n">genus</span><span class="p">(</span><span class="n">g</span><span class="p">)</span> <span class="k">for</span> <span class="n">g</span> <span class="ow">in</span> <span class="n">graphs</span><span class="p">(</span><span class="mi">6</span><span class="p">)</span> <span class="k">if</span> <span class="n">g</span><span class="o">.</span><span class="n">is_connected</span><span class="p">()]</span><span class="o">.</span><span class="n">count</span><span class="p">(</span><span class="mi">1</span><span class="p">)</span>
<span class="go">13</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">FlowerSnark</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">genus</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>  <span class="c1"># see [1]</span>
<span class="go">2</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">BubbleSortGraph</span><span class="p">(</span><span class="mi">4</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">genus</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>
<span class="go">0</span>
<span class="gp">sage: </span><span class="n">G</span> <span class="o">=</span> <span class="n">graphs</span><span class="o">.</span><span class="n">OddGraph</span><span class="p">(</span><span class="mi">3</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">genus</span><span class="p">(</span><span class="n">G</span><span class="p">)</span>
<span class="go">1</span>
</pre></div>
</div>
<p>REFERENCES:</p>
<p>[1] <a class="reference external" href="http://www.springerlink.com/content/0776127h0r7548v7/">http://www.springerlink.com/content/0776127h0r7548v7/</a></p>
</dd></dl>

</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="matchpoly.html"
                          title="previous chapter">Matching Polynomial</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="lovasz_theta.html"
                          title="next chapter">Lovász theta-function of graphs</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../_sources/sage/graphs/genus.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="lovasz_theta.html" title="Lovász theta-function of graphs"
             >next</a> |</li>
        <li class="right" >
          <a href="matchpoly.html" title="Matching Polynomial"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../index.html">Graph Theory</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Genus</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>