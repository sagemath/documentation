.. _spkg_tzlocal:

tzlocal: Python timezone information for the local timezone
===========================================================

Description
-----------

tzinfo object for the local timezone

Type
----

standard


Version Information
-------------------

package-version.txt::

    2.1

install-requires.txt::

    tzlocal >=2.1


Equivalent System Packages
--------------------------

conda:

.. CODE-BLOCK:: bash

    $ conda install tzlocal


macports:
install the following packages: py-tzlocal

opensuse:

.. CODE-BLOCK:: bash

    $ sudo zypper install python3-tzlocal


void:

.. CODE-BLOCK:: bash

    $ sudo xbps-install  python3-tzlocal


See https://repology.org/project/tzlocal/versions, https://repology.org/project/python:tzlocal/versions


However, these system packages will not be used for building Sage
because using Python site-packages is not supported by the Sage distribution;
see :trac:`29023`

