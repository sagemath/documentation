.. _spkg_pyzmq:

pyzmq: Python bindings for the zeromq networking library
========================================================

Description
-----------

Python bindings for the zeromq networking library.

License
-------

LGPLv3+


Upstream Contact
----------------

http://www.zeromq.org

Dependencies
------------

-  Python
-  Cython
-  zeromq


Special Update/Build Instructions
---------------------------------

None.

Type
----

standard


Version Information
-------------------

package-version.txt::

    22.3.0

install-requires.txt::

    pyzmq >=19.0.2


Equivalent System Packages
--------------------------

conda:

.. CODE-BLOCK:: bash

    $ conda install pyzmq


opensuse:

.. CODE-BLOCK:: bash

    $ sudo zypper install python3-pyzmq


void:

.. CODE-BLOCK:: bash

    $ sudo xbps-install  python3-pyzmq


See https://repology.org/project/pyzmq/versions, https://repology.org/project/python:pyzmq/versions


However, these system packages will not be used for building Sage
because using Python site-packages is not supported by the Sage distribution;
see :trac:`29023`

