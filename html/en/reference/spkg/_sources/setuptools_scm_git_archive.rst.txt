.. _spkg_setuptools_scm_git_archive:

setuptools_scm_git_archive: setuptools_scm plugin for git archives
==================================================================

Description
-----------

setuptools_scm plugin for git archives

License
-------

MIT

Upstream Contact
----------------

https://pypi.org/project/setuptools-scm-git-archive/


Type
----

standard


Version Information
-------------------

package-version.txt::

    1.1

install-requires.txt::

    setuptools-scm-git-archive


Equivalent System Packages
--------------------------


However, these system packages will not be used for building Sage
because using Python site-packages is not supported by the Sage distribution;
see :trac:`29023`

