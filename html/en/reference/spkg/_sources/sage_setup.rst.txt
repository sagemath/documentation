.. _spkg_sage_setup:

sage-setup: Build system of the SageMath library
================================================

This is the build system of the Sage library, based on setuptools.

Type
----

standard


Version Information
-------------------

package-version.txt::

    9.6

install-requires.txt::

    sage-setup ~= 9.5.b6


Equivalent System Packages
--------------------------


However, these system packages will not be used for building Sage
because using Python site-packages is not supported by the Sage distribution;
see :trac:`29023`

