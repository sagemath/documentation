
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Example of facade set &#8212; Category Framework</title>
    <link rel="stylesheet" type="text/css" href="../../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/thebelab.css" />
    
    <script data-url_root="../../../" id="documentation_options" src="../../../../_static/documentation_options.js"></script>
    <script src="../../../../_static/jquery.js"></script>
    <script src="../../../../_static/underscore.js"></script>
    <script src="../../../../_static/doctools.js"></script>
    <script src="../../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../../genindex.html" />
    <link rel="search" title="Search" href="../../../../search.html" />
    <link rel="next" title="Examples of finite Coxeter groups" href="finite_coxeter_groups.html" />
    <link rel="prev" title="Examples of CW complexes" href="cw_complexes.html" />
  <link rel="icon" href="../../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="finite_coxeter_groups.html" title="Examples of finite Coxeter groups"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="cw_complexes.html" title="Examples of CW complexes"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Category Framework</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Example of facade set</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="example-of-facade-set">
<span id="sage-categories-examples-facade-sets"></span><h1>Example of facade set<a class="headerlink" href="#example-of-facade-set" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.categories.examples.facade_sets"></span><dl class="py class">
<dt class="sig sig-object py" id="sage.categories.examples.facade_sets.IntegersCompletion">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.categories.examples.facade_sets.</span></span><span class="sig-name descname"><span class="pre">IntegersCompletion</span></span><a class="headerlink" href="#sage.categories.examples.facade_sets.IntegersCompletion" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference external" href="../../../../structure/sage/structure/unique_representation.html#sage.structure.unique_representation.UniqueRepresentation" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.unique_representation.UniqueRepresentation</span></code></a>, <a class="reference external" href="../../../../structure/sage/structure/parent.html#sage.structure.parent.Parent" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.parent.Parent</span></code></a></p>
<p>An example of a facade parent: the set of integers completed with
<span class="math notranslate nohighlight">\(+-\infty\)</span></p>
<p>This class illustrates a minimal implementation of a facade parent
that models the union of several other parents.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">S</span> <span class="o">=</span> <span class="n">Sets</span><span class="p">()</span><span class="o">.</span><span class="n">Facade</span><span class="p">()</span><span class="o">.</span><span class="n">example</span><span class="p">(</span><span class="s2">&quot;union&quot;</span><span class="p">);</span> <span class="n">S</span>
<span class="go">An example of a facade set: the integers completed by +-infinity</span>
</pre></div>
</div>
</dd></dl>

<dl class="py class">
<dt class="sig sig-object py" id="sage.categories.examples.facade_sets.PositiveIntegerMonoid">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.categories.examples.facade_sets.</span></span><span class="sig-name descname"><span class="pre">PositiveIntegerMonoid</span></span><a class="headerlink" href="#sage.categories.examples.facade_sets.PositiveIntegerMonoid" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference external" href="../../../../structure/sage/structure/unique_representation.html#sage.structure.unique_representation.UniqueRepresentation" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.unique_representation.UniqueRepresentation</span></code></a>, <a class="reference external" href="../../../../structure/sage/structure/parent.html#sage.structure.parent.Parent" title="(in Parents and Elements v9.6)"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.structure.parent.Parent</span></code></a></p>
<p>An example of a facade parent: the positive integers viewed as a
multiplicative monoid</p>
<p>This class illustrates a minimal implementation of a facade parent
which models a subset of a set.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">S</span> <span class="o">=</span> <span class="n">Sets</span><span class="p">()</span><span class="o">.</span><span class="n">Facade</span><span class="p">()</span><span class="o">.</span><span class="n">example</span><span class="p">();</span> <span class="n">S</span>
<span class="go">An example of facade set: the monoid of positive integers</span>
</pre></div>
</div>
</dd></dl>

</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="cw_complexes.html"
                          title="previous chapter">Examples of CW complexes</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="finite_coxeter_groups.html"
                          title="next chapter">Examples of finite Coxeter groups</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../../_sources/sage/categories/examples/facade_sets.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="finite_coxeter_groups.html" title="Examples of finite Coxeter groups"
             >next</a> |</li>
        <li class="right" >
          <a href="cw_complexes.html" title="Examples of CW complexes"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../index.html">Category Framework</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Example of facade set</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>