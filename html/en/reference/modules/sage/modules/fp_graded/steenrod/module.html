
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Finitely presented graded modules over the Steenrod algebra &#8212; Modules</title>
    <link rel="stylesheet" type="text/css" href="../../../../../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../../../../../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../../../../../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../../../../../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../../../../../_static/thebelab.css" />
    
    <script data-url_root="../../../../" id="documentation_options" src="../../../../../_static/documentation_options.js"></script>
    <script src="../../../../../_static/jquery.js"></script>
    <script src="../../../../../_static/underscore.js"></script>
    <script src="../../../../../_static/doctools.js"></script>
    <script src="../../../../../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../../../../../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../../../../../genindex.html" />
    <link rel="search" title="Search" href="../../../../../search.html" />
    <link rel="next" title="Homomorphisms of finitely presented modules over the Steenrod algebra" href="morphism.html" />
    <link rel="prev" title="Homsets of finitely presented graded modules" href="../homspace.html" />
  <link rel="icon" href="../../../../../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../../../../../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../../../../../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="morphism.html" title="Homomorphisms of finitely presented modules over the Steenrod algebra"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="../homspace.html" title="Homsets of finitely presented graded modules"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../../index.html">Modules</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Finitely presented graded modules over the Steenrod algebra</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="finitely-presented-graded-modules-over-the-steenrod-algebra">
<span id="sage-modules-fp-graded-steenrod-module"></span><h1>Finitely presented graded modules over the Steenrod algebra<a class="headerlink" href="#finitely-presented-graded-modules-over-the-steenrod-algebra" title="Permalink to this headline">¶</a></h1>
<span class="target" id="module-sage.modules.fp_graded.steenrod.module"></span><p>This package allows the user to define finitely presented modules
over the mod <span class="math notranslate nohighlight">\(p\)</span> Steenrod algebra, elements of them, and
morphisms between them.  Methods are provided for doing basic homological
algebra, e.g. computing kernels and images of homomorphisms, and finding
free resolutions of modules.</p>
<p class="rubric">Theoretical background</p>
<p>The category of finitely presented graded modules over an arbitrary
non-Noetherian graded ring <span class="math notranslate nohighlight">\(R\)</span> is not abelian in general, since
kernels of homomorphisms are not necessarily finitely presented.</p>
<p>The mod <span class="math notranslate nohighlight">\(p\)</span> Steenrod algebra is non-Noetherian, but it is the
union of a countable set of finite sub-Hopf algebras
(<a class="reference internal" href="../../../../../references/index.html#mar1983" id="id1"><span>[Mar1983]</span></a> Ch. 15, Sect. 1, Prop 7). It is therefore an example of a
<span class="math notranslate nohighlight">\(P\)</span>-algebra (<a class="reference internal" href="../../../../../references/index.html#mar1983" id="id2"><span>[Mar1983]</span></a> Ch. 13).</p>
<p>Any finitely presented module over the Steenrod algebra is defined
over one of these finite sub-Hopf algebras.  Similarly, any
homomorphism between finitely presented modules over the Steenrod
algebra is defined over a finite sub-Hopf algebra of the Steenrod
algebra.  Further, tensoring up from the category of modules over a
sub-Hopf algebra is an exact functor, since the Steenrod algebra is
free over any sub-Hopf algebra.</p>
<p>It follows that kernels, cokernels, images, and, more generally, any finite
limits or colimits can be computed in the category of modules over the
Steenrod algebra by working in the category of modules over an appropriate
finite sub-Hopf algebra.</p>
<p>It is also the case that presentations of modules and the images of the
generators of the domain of a homomorphism are the same over the sub-Hopf
algebra and over the whole Steenrod algebra, so that the tensoring up is
entirely implicit and requires no computation.</p>
<p>The definitions and computations carried out by this package are thus done
as follows.   First, find a small finite sub-Hopf algebra over which the
computation can be done.   Then, carry out the calculation there, where it
is a finite problem and can be reduced to linear algebra over a finite
prime field.</p>
<p>For examples, see the <a class="reference external" href="../../../../../../thematic_tutorials/steenrod_algebra_modules.html">Steenrod algebra modules</a>
thematic tutorial.</p>
<p>AUTHORS:</p>
<ul class="simple">
<li><p>Robert R. Bruner, Michael J. Catanzaro (2012): Initial version.</p></li>
<li><p>Sverre Lunoee–Nielsen and Koen van Woerden (2019-11-29): Updated the
original software to Sage version 8.9.</p></li>
<li><p>Sverre Lunoee–Nielsen (2020-07-01): Refactored the code and added
new documentation and tests.</p></li>
</ul>
<dl class="py class">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodFPModule">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.modules.fp_graded.steenrod.module.</span></span><span class="sig-name descname"><span class="pre">SteenrodFPModule</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">j</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">names</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodFPModule" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference internal" href="../module.html#sage.modules.fp_graded.module.FPModule" title="sage.modules.fp_graded.module.FPModule"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.modules.fp_graded.module.FPModule</span></code></a>, <a class="reference internal" href="#sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin" title="sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin</span></code></a></p>
<p>Create a finitely presented module over the Steenrod algebra.</p>
<div class="admonition seealso">
<p class="admonition-title">See also</p>
<p>The thematic tutorial on <a class="reference external" href="../../../../../../thematic_tutorials/steenrod_algebra_modules.html">Steenrod algebra modules</a>.</p>
</div>
<p>INPUT:</p>
<p>One of the following:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">arg0</span></code> – a morphism such that the module is the cokernel, or
a free graded module, in which case the output is the same
module, viewed as finitely presented</p></li>
</ul>
<p>Otherwise:</p>
<ul>
<li><p><code class="docutils literal notranslate"><span class="pre">arg0</span></code> – the graded connected algebra over which the module is
defined; this algebra must be equipped with a graded basis</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">generator_degrees</span></code> – tuple of integer degrees</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">relations</span></code> – tuple of relations; a relation is a tuple of
coefficients <span class="math notranslate nohighlight">\((c_1, \ldots, c_n)\)</span>, ordered so that they
correspond to the module generators, that is, such a tuple
corresponds to the relation</p>
<div class="math notranslate nohighlight">
\[c_1 g_1 + \ldots + c_n g_n = 0\]</div>
<p>if the generators are <span class="math notranslate nohighlight">\((g_1, \ldots, g_n)\)</span></p>
</li>
</ul>
<dl class="py method">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodFPModule.resolution">
<span class="sig-name descname"><span class="pre">resolution</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">k</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">top_dim</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">verbose</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">False</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodFPModule.resolution" title="Permalink to this definition">¶</a></dt>
<dd><p>A free resolution of <code class="docutils literal notranslate"><span class="pre">self</span></code> of the given length.</p>
<p>INPUT:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">k</span></code> – non-negative integer</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">top_dim</span></code> – (optional) stop the computation at this degree</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">verbose</span></code> – (default: <code class="docutils literal notranslate"><span class="pre">False</span></code>) whether log messages are printed</p></li>
</ul>
<p>OUTPUT:</p>
<p>A list of homomorphisms <span class="math notranslate nohighlight">\([\epsilon, f_1, \ldots, f_k]\)</span> such that</p>
<div class="math notranslate nohighlight">
\[\begin{split}\begin{gathered}
f_i: F_i \to F_{i-1} \text{ for } 1\leq i\leq k, \\
\epsilon: F_0\to M,
\end{gathered}\end{split}\]</div>
<p>where each <span class="math notranslate nohighlight">\(F_i\)</span> is a finitely generated free module, and the
sequence</p>
<div class="math notranslate nohighlight">
\[F_k \xrightarrow{f_k} F_{k-1} \xrightarrow{f_{k-1}} \ldots
\rightarrow F_0 \xrightarrow{\epsilon} M \rightarrow 0\]</div>
<p>is exact. Note that the 0th element in this list is the map
<span class="math notranslate nohighlight">\(\epsilon\)</span>, and the rest of the maps are between free
modules.</p>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.modules.fp_graded.steenrod.module</span> <span class="kn">import</span> <span class="n">SteenrodFPModule</span>
<span class="gp">sage: </span><span class="n">A</span> <span class="o">=</span> <span class="n">SteenrodAlgebra</span><span class="p">(</span><span class="mi">2</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Hko</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">],</span> <span class="p">[[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">1</span><span class="p">)],</span> <span class="p">[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">2</span><span class="p">)]])</span>

<span class="gp">sage: </span><span class="n">res</span> <span class="o">=</span> <span class="n">Hko</span><span class="o">.</span><span class="n">resolution</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span> <span class="n">verbose</span><span class="o">=</span><span class="kc">True</span><span class="p">)</span>
<span class="go">Computing f_1 (1/5)</span>
<span class="go">Computing f_2 (2/5)</span>
<span class="go">Computing using the profile:</span>
<span class="go">(2, 1)</span>
<span class="go">Resolving the kernel in the range of dimensions [1, 8]: 1 2 3 4 5 6 7 8.</span>
<span class="go">Computing f_3 (3/5)</span>
<span class="go">Computing using the profile:</span>
<span class="go">(2, 1)</span>
<span class="go">Resolving the kernel in the range of dimensions [2, 10]: 2 3 4 5 6 7 8 9 10.</span>
<span class="go">Computing f_4 (4/5)</span>
<span class="go">Computing using the profile:</span>
<span class="go">(2, 1)</span>
<span class="go">Resolving the kernel in the range of dimensions [3, 13]: 3 4 5 6 7 8 9 10 11 12 13.</span>
<span class="go">Computing f_5 (5/5)</span>
<span class="go">Computing using the profile:</span>
<span class="go">(2, 1)</span>
<span class="go">Resolving the kernel in the range of dimensions [4, 18]: 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18.</span>
<span class="gp">sage: </span><span class="p">[</span><span class="n">x</span><span class="o">.</span><span class="n">domain</span><span class="p">()</span> <span class="k">for</span> <span class="n">x</span> <span class="ow">in</span> <span class="n">res</span><span class="p">]</span>
<span class="go">[Free graded left module on 1 generator over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Free graded left module on 2 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Free graded left module on 2 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Free graded left module on 2 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Free graded left module on 3 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Free graded left module on 4 generators over mod 2 Steenrod algebra, milnor basis]</span>
</pre></div>
</div>
<p>When there are no relations, the resolution is trivial:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">M</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">M</span><span class="o">.</span><span class="n">resolution</span><span class="p">(</span><span class="mi">4</span><span class="p">)</span>
<span class="go">[Module endomorphism of Free graded left module on 1 generator over mod 2 Steenrod algebra, milnor basis</span>
<span class="go">   Defn: g[0] |--&gt; g[0],</span>
<span class="go"> Module morphism:</span>
<span class="go">   From: Free graded left module on 0 generators over mod 2 Steenrod algebra, milnor basis</span>
<span class="go">   To:   Free graded left module on 1 generator over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Module endomorphism of Free graded left module on 0 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Module endomorphism of Free graded left module on 0 generators over mod 2 Steenrod algebra, milnor basis,</span>
<span class="go"> Module endomorphism of Free graded left module on 0 generators over mod 2 Steenrod algebra, milnor basis]</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

<dl class="py class">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodFreeModule">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.modules.fp_graded.steenrod.module.</span></span><span class="sig-name descname"><span class="pre">SteenrodFreeModule</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">algebra</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">generator_degrees</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">category</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">names</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">None</span></span></em>, <em class="sig-param"><span class="o"><span class="pre">**</span></span><span class="n"><span class="pre">kwds</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodFreeModule" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <a class="reference internal" href="../free_module.html#sage.modules.fp_graded.free_module.FreeGradedModule" title="sage.modules.fp_graded.free_module.FreeGradedModule"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.modules.fp_graded.free_module.FreeGradedModule</span></code></a>, <a class="reference internal" href="#sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin" title="sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin"><code class="xref py py-class docutils literal notranslate"><span class="pre">sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin</span></code></a></p>
</dd></dl>

<dl class="py class">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin">
<em class="property"><span class="pre">class</span><span class="w"> </span></em><span class="sig-prename descclassname"><span class="pre">sage.modules.fp_graded.steenrod.module.</span></span><span class="sig-name descname"><span class="pre">SteenrodModuleMixin</span></span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal notranslate"><span class="pre">object</span></code></p>
<p>Mixin class for common methods of the Steenrod algebra modules.</p>
<dl class="py method">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin.export_module_definition">
<span class="sig-name descname"><span class="pre">export_module_definition</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">powers_of_two_only</span></span><span class="o"><span class="pre">=</span></span><span class="default_value"><span class="pre">True</span></span></em><span class="sig-paren">)</span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin.export_module_definition" title="Permalink to this definition">¶</a></dt>
<dd><p>Return the module to the input
<a class="reference external" href="http://www.math.wayne.edu/~rrb/cohom/modfmt.html">format used by R. Bruner’s Ext software</a> as a string.</p>
<p>INPUT:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">powers_of_two_only</span></code> – boolean (default: <code class="docutils literal notranslate"><span class="pre">True</span></code>); if the
output should contain the action of all Steenrod squaring operations
(restricted by the profile), or only the action of the operations
of degree equal to a power of two</p></li>
</ul>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.modules.fp_graded.steenrod.module</span> <span class="kn">import</span> <span class="n">SteenrodFPModule</span>
<span class="gp">sage: </span><span class="n">A1</span> <span class="o">=</span> <span class="n">algebra</span><span class="o">=</span><span class="n">SteenrodAlgebra</span><span class="p">(</span><span class="n">p</span><span class="o">=</span><span class="mi">2</span><span class="p">,</span> <span class="n">profile</span><span class="o">=</span><span class="p">[</span><span class="mi">2</span><span class="p">,</span><span class="mi">1</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">M</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A1</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">])</span>
<span class="gp">sage: </span><span class="nb">print</span><span class="p">(</span><span class="n">M</span><span class="o">.</span><span class="n">export_module_definition</span><span class="p">())</span>
<span class="go">8 0 1 2 3 3 4 5 6</span>
<span class="go">0 1 1 1</span>
<span class="go">2 1 1 4</span>
<span class="go">3 1 1 5</span>
<span class="go">6 1 1 7</span>
<span class="go">0 2 1 2</span>
<span class="go">1 2 2 3 4</span>
<span class="go">2 2 1 5</span>
<span class="go">3 2 1 6</span>
<span class="go">4 2 1 6</span>
<span class="go">5 2 1 7</span>
<span class="gp">sage: </span><span class="n">N</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A1</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">],</span> <span class="p">[[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">1</span><span class="p">)]])</span>
<span class="gp">sage: </span><span class="nb">print</span><span class="p">(</span><span class="n">N</span><span class="o">.</span><span class="n">export_module_definition</span><span class="p">())</span>
<span class="go">4 0 2 3 5</span>
<span class="go">1 1 1 2</span>
<span class="go">0 2 1 1</span>
<span class="go">2 2 1 3</span>
<span class="gp">sage: </span><span class="nb">print</span><span class="p">(</span><span class="n">N</span><span class="o">.</span><span class="n">export_module_definition</span><span class="p">(</span><span class="n">powers_of_two_only</span><span class="o">=</span><span class="kc">False</span><span class="p">))</span>
<span class="go">4 0 2 3 5</span>
<span class="go">1 1 1 2</span>
<span class="go">0 2 1 1</span>
<span class="go">2 2 1 3</span>
<span class="go">0 3 1 2</span>
<span class="gp">sage: </span><span class="n">A2</span> <span class="o">=</span> <span class="n">SteenrodAlgebra</span><span class="p">(</span><span class="n">p</span><span class="o">=</span><span class="mi">2</span><span class="p">,</span> <span class="n">profile</span><span class="o">=</span><span class="p">[</span><span class="mi">3</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">1</span><span class="p">])</span>
<span class="gp">sage: </span><span class="n">Hko</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A2</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">],</span> <span class="p">[[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">1</span><span class="p">)],</span> <span class="p">[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">2</span><span class="p">)]])</span>
<span class="gp">sage: </span><span class="nb">print</span><span class="p">(</span><span class="n">Hko</span><span class="o">.</span><span class="n">export_module_definition</span><span class="p">())</span>
<span class="go">8 0 4 6 7 10 11 13 17</span>
<span class="go">2 1 1 3</span>
<span class="go">4 1 1 5</span>
<span class="go">1 2 1 2</span>
<span class="go">5 2 1 6</span>
<span class="go">0 4 1 1</span>
<span class="go">2 4 1 4</span>
<span class="go">3 4 1 5</span>
<span class="go">6 4 1 7</span>
</pre></div>
</div>
</dd></dl>

<dl class="py method">
<dt class="sig sig-object py" id="sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin.profile">
<span class="sig-name descname"><span class="pre">profile</span></span><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="headerlink" href="#sage.modules.fp_graded.steenrod.module.SteenrodModuleMixin.profile" title="Permalink to this definition">¶</a></dt>
<dd><p>Return a finite profile over which <code class="docutils literal notranslate"><span class="pre">self</span></code> can be defined.</p>
<p>Any finitely presented module over the Steenrod algebra can be
defined over a finite-dimensional sub-Hopf algebra, and this
method identifies such a sub-Hopf algebra and returns its
profile function.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>The profile produced by this function is reasonably small
but is not guaranteed to be minimal.</p>
</div>
<p>EXAMPLES:</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.modules.fp_graded.steenrod.module</span> <span class="kn">import</span> <span class="n">SteenrodFPModule</span>
<span class="gp">sage: </span><span class="n">A</span> <span class="o">=</span> <span class="n">SteenrodAlgebra</span><span class="p">(</span><span class="mi">2</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">M</span> <span class="o">=</span> <span class="n">SteenrodFPModule</span><span class="p">(</span><span class="n">A</span><span class="p">,</span> <span class="p">[</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">],</span> <span class="p">[[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">2</span><span class="p">),</span><span class="n">Sq</span><span class="p">(</span><span class="mi">1</span><span class="p">)],[</span><span class="mi">0</span><span class="p">,</span><span class="n">Sq</span><span class="p">(</span><span class="mi">2</span><span class="p">)],[</span><span class="n">Sq</span><span class="p">(</span><span class="mi">3</span><span class="p">),</span><span class="mi">0</span><span class="p">]])</span>
<span class="gp">sage: </span><span class="n">M</span><span class="o">.</span><span class="n">profile</span><span class="p">()</span>
<span class="go">(2, 1)</span>
</pre></div>
</div>
</dd></dl>

</dd></dl>

</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="../homspace.html"
                          title="previous chapter">Homsets of finitely presented graded modules</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="morphism.html"
                          title="next chapter">Homomorphisms of finitely presented modules over the Steenrod algebra</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../../../../_sources/sage/modules/fp_graded/steenrod/module.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../../../../../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../../../../../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="morphism.html" title="Homomorphisms of finitely presented modules over the Steenrod algebra"
             >next</a> |</li>
        <li class="right" >
          <a href="../homspace.html" title="Homsets of finitely presented graded modules"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../../../../../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../../../../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../../../../../index.html">Reference Manual</a> &#187;
      
      <a href="../../../../index.html">Modules</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Finitely presented graded modules over the Steenrod algebra</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>