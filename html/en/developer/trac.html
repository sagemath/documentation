
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>The Sage Trac Server &#8212; Developer&#39;s Guide</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="_static/thebelab.css" />
    
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Using Git with the Sage Trac Server" href="manual_git.html" />
    <link rel="prev" title="Sage Development Process" href="walk_through.html" />
  <link rel="icon" href="_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="manual_git.html" title="Using Git with the Sage Trac Server"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="walk_through.html" title="Sage Development Process"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="index.html">Developer&#39;s Guide</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">The Sage Trac Server</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="the-sage-trac-server">
<span id="chapter-sage-trac"></span><h1>The Sage Trac Server<a class="headerlink" href="#the-sage-trac-server" title="Permalink to this headline">¶</a></h1>
<p>Sometimes you will only want to work on local changes to Sage, for
your own private needs.  However, typically it is beneficial to
share code and ideas with others; the manner in which the
<a class="reference external" href="https://www.sagemath.org">Sage project</a> does this (as well as fixing
bugs and upgrading components) is in a very collaborative and
public setting on <a class="reference external" href="https://trac.sagemath.org">the Sage Trac server</a>
(the Sage bug and enhancement tracker).</p>
<p>The purpose of the Sage trac server is to</p>
<ol class="arabic simple">
<li><p>Provide a place for discussion on issues and store a permanent
record.</p></li>
<li><p>Provide a repository of source code and all proposed changes.</p></li>
<li><p>Link these two together.</p></li>
</ol>
<p>There is also a <a class="reference external" href="https://trac.sagemath.org/wiki">wiki</a> for more general
organizational web pages, like Sage development workshops.</p>
<p>Thus if you find a bug in Sage, if you have new code to submit, want
to review new code already written but not yet included in Sage, or if
you have corrections for the documentation, you should post on the
trac server. Items on the server are called <em>tickets</em>, and anyone may
search or browse the tickets. For a list of recent changes, just visit
the <a class="reference external" href="https://trac.sagemath.org/timeline">Sage trac timeline</a>.</p>
<section id="obtaining-an-account">
<span id="section-trac-account"></span><h2>Obtaining an Account<a class="headerlink" href="#obtaining-an-account" title="Permalink to this headline">¶</a></h2>
<p><strong>New:</strong> Previously, it was necessary to manually request a Trac account in
order to post anything to Sage’s Trac.  Now, if you have a GitHub account, you
may log in using it to create and comment on tickets, and edit wiki pages on
Sage’s Trac.</p>
<p>A manual account request is currently only necessary if you prefer not to
use GitHub or if you want to log into the old <a class="reference external" href="https://wiki.sagemath.org">Sage Wiki</a>.  This may change as well in the future.</p>
<p>To obtain a non-GitHub account, send an email to
<code class="docutils literal notranslate"><span class="pre">sage-trac-account&#64;googlegroups.com</span></code> containing:</p>
<ul class="simple">
<li><p>your full name,</p></li>
<li><p>preferred username,</p></li>
<li><p>contact email,</p></li>
<li><p>and reason for needing a trac account</p></li>
</ul>
<p>Your trac account also grants you access to the <a class="reference external" href="https://wiki.sagemath.org">sage wiki</a>. Make sure you understand the review process, and
the procedures for opening and closing tickets before making changes. The
remainder of this chapter contains various guidelines on using the trac server.</p>
</section>
<section id="trac-authentication-through-ssh">
<h2>Trac authentication through SSH<a class="headerlink" href="#trac-authentication-through-ssh" title="Permalink to this headline">¶</a></h2>
<p>There are two avenues to prove to the trac server that you are who you
claim to be. First, to change the ticket web pages you need to log in
to trac using a username/password. Second, there is public key
cryptography used by git when copying new source files to the
repository. This section will show you how to set up both.</p>
<section id="generating-and-uploading-your-ssh-keys">
<h3>Generating and Uploading your SSH Keys<a class="headerlink" href="#generating-and-uploading-your-ssh-keys" title="Permalink to this headline">¶</a></h3>
<p>The git installation on the development server uses SSH keys to decide if and
where you are allowed to upload code. No SSH key is required to report a bug or
comment on a ticket, but as soon as you want to contribute code yourself you
need to provide trac with the public half of your own personal key.
Details are described in the following two sections.</p>
</section>
<section id="generating-your-ssh-keys">
<h3>Generating your SSH Keys<a class="headerlink" href="#generating-your-ssh-keys" title="Permalink to this headline">¶</a></h3>
<p>If you don’t have a private key yet, you can
create it with the <code class="docutils literal notranslate"><span class="pre">ssh-keygen</span></code> tool:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost ~]$ </span>ssh-keygen
<span class="go">Generating public/private rsa key pair.</span>
<span class="go">Enter file in which to save the key (/home/user/.ssh/id_rsa):</span>
<span class="go">Enter passphrase (empty for no passphrase):</span>
<span class="go">Enter same passphrase again:</span>
<span class="go">Your identification has been saved in /home/user/.ssh/id_rsa.</span>
<span class="go">Your public key has been saved in /home/user/.ssh/id_rsa.pub.</span>
<span class="go">The key fingerprint is:</span>
<span class="go">ce:32:b3:de:38:56:80:c9:11:f0:b3:88:f2:1c:89:0a user@localhost</span>
<span class="go">The key&#39;s randomart image is:</span>
<span class="go">+--[ RSA 2048]----+</span>
<span class="go">|  ....           |</span>
<span class="go">|   ..            |</span>
<span class="go">|   .o+           |</span>
<span class="go">| o o+o.          |</span>
<span class="go">|E + .  .S        |</span>
<span class="go">|+o .   o.        |</span>
<span class="go">|. o   +.o        |</span>
<span class="go">|      oB         |</span>
<span class="go">|     o+..        |</span>
<span class="go">+-----------------+</span>
</pre></div>
</div>
<p>This will generate a new random private RSA key
in the <code class="docutils literal notranslate"><span class="pre">.ssh</span></code> folder in your home directory. By default, they are</p>
<dl class="simple">
<dt><code class="docutils literal notranslate"><span class="pre">~/.ssh/id_rsa</span></code></dt><dd><p>Your private key. Keep safe. <strong>Never</strong> hand it out to anybody.</p>
</dd>
<dt><code class="docutils literal notranslate"><span class="pre">~/.ssh/id_rsa.pub</span></code></dt><dd><p>The corresponding public key. This and only this file can be safely
disclosed to third parties.</p>
</dd>
</dl>
<p>The <code class="docutils literal notranslate"><span class="pre">ssh-keygen</span></code> tool will let you generate a key with a different
file name, or protect it with a passphrase. Depending on how much you
trust your own computer or system administrator, you can leave the
passphrase empty to be able to login without any human intervention.</p>
<p>If you have accounts on multiple computers you can use the SSH keys to
log in. Just copy the <strong>public</strong> key file (ending in <code class="docutils literal notranslate"><span class="pre">.pub</span></code>) to
<code class="docutils literal notranslate"><span class="pre">~/.ssh/authorized_keys</span></code> on the remote computer and make sure that
the file is only read/writeable by yourself. Voila, the next time you
ssh into that machine you don’t have to provide your password.</p>
</section>
<section id="linking-your-public-key-to-your-trac-account">
<span id="section-trac-ssh-key"></span><h3>Linking your Public Key to your Trac Account<a class="headerlink" href="#linking-your-public-key-to-your-trac-account" title="Permalink to this headline">¶</a></h3>
<p>The Sage trac server needs to know one of your public keys. You can
upload it in the preferences, that is</p>
<ol class="arabic simple">
<li><p>Go to <a class="reference external" href="https://trac.sagemath.org">https://trac.sagemath.org</a></p></li>
<li><p>Log in with your trac username/password</p></li>
<li><p>Click on “Preferences”</p></li>
<li><p>Go to the “SSH Keys” tab</p></li>
<li><p>Paste the content of your public key file
(e.g. <code class="docutils literal notranslate"><span class="pre">~/.ssh/id_rsa.pub</span></code>)</p></li>
<li><p>Click on “Save changes”</p></li>
</ol>
<p>Note that this does <strong>not</strong> allow you to ssh into any account on trac,
it is only used to authenticate you to the gitolite installation on
trac. You can test that you are being authenticated correctly by
issuing some basic gitolite commands, for example:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost ~]$ </span>ssh git@trac.sagemath.org info
<span class="go">hello user, this is git@trac running gitolite3 (unknown) on git 1.7.9.5</span>

<span class="go"> R W      sage</span>
<span class="gp">[user@localhost ~]$ </span>ssh git@trac.sagemath.org <span class="nb">help</span>
<span class="go">hello user, this is gitolite3 (unknown) on git 1.7.9.5</span>

<span class="go">list of remote commands available:</span>

<span class="go">    desc</span>
<span class="go">    help</span>
<span class="go">    info</span>
<span class="go">    perms</span>
<span class="go">    writable</span>
</pre></div>
</div>
</section>
</section>
<section id="reporting-bugs">
<span id="trac-bug-report"></span><h2>Reporting Bugs<a class="headerlink" href="#reporting-bugs" title="Permalink to this headline">¶</a></h2>
<p>If you think you have found a bug in Sage, here is the procedure:</p>
<ul>
<li><p>Search through our Google groups for postings related to your possible bug (it
may have been fixed/reported already):</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">sage-devel</span></code>: <a class="reference external" href="https://groups.google.com/group/sage-devel">https://groups.google.com/group/sage-devel</a></p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">sage-support</span></code>: <a class="reference external" href="https://groups.google.com/group/sage-support">https://groups.google.com/group/sage-support</a></p></li>
</ul>
<p>Similarly, you can search <a class="reference internal" href="#chapter-sage-trac"><span class="std std-ref">The Sage Trac Server</span></a> to see if anyone else has
opened a ticket about your bug.</p>
</li>
<li><p>If you do not find anything, and you are not sure that you have found a bug,
ask about it on <a class="reference external" href="https://groups.google.com/group/sage-devel">sage-devel</a>. A
bug report should contain:</p>
<ul class="simple">
<li><p>An explicit and <strong>reproducible example</strong> illustrating your bug (and/or the
steps required to reproduce the buggy behavior).</p></li>
<li><p>The <strong>version</strong> of Sage you run, as well as the version of the optional
packages that may be involved in the bug.</p></li>
<li><p>Describe your <strong>operating system</strong> as accurately as you can and your
architecture (32-bit, 64-bit, …).</p></li>
</ul>
</li>
<li><p>You might be asked to open a new ticket. In this case, follow the
<a class="reference internal" href="#section-trac-new-ticket"><span class="std std-ref">Guidelines for Opening Tickets</span></a>.</p></li>
</ul>
<p>Thank you in advance for reporting bugs to improve Sage in the future!</p>
</section>
<section id="guidelines-for-opening-tickets">
<span id="section-trac-new-ticket"></span><h2>Guidelines for Opening Tickets<a class="headerlink" href="#guidelines-for-opening-tickets" title="Permalink to this headline">¶</a></h2>
<p>In addition to bug reports (see <a class="reference internal" href="#trac-bug-report"><span class="std std-ref">Reporting Bugs</span></a>), you should also open a
ticket if you have some new code that makes Sage a better tool. If you have a
feature request, start a discussion on <code class="docutils literal notranslate"><span class="pre">sage-devel</span></code> first, and then if there
seems to be general agreement that you have a good idea, open a ticket
describing the idea.</p>
<ul class="simple">
<li><p>Do you already have a <strong>trac account</strong>? If not, <a class="reference internal" href="#section-trac-account"><span class="std std-ref">click here</span></a>.</p></li>
</ul>
<p><strong>Before</strong> opening a new ticket, consider the following points:</p>
<ul class="simple">
<li><p>Make sure that nobody else has opened a ticket about the same or closely
related issue.</p></li>
<li><p>It is much better to open several specific tickets than one that
is very broad. Indeed, a single ticket which deals with lots of
different issues can be quite problematic, and should be avoided.</p></li>
<li><p>Be precise: If foo does not work on OS X but is fine on Linux,
mention that in the title. Use the keyword option so that
searches will pick up the issue.</p></li>
<li><p>The problem described in the ticket must be solvable. For
example, it would be silly to open a ticket whose purpose was
“Make Sage the best mathematical software in the world”. There is
no metric to measure this properly and it is highly subjective.</p></li>
<li><p>For bug reports: the ticket’s description should contain the information
described at <a class="reference internal" href="#trac-bug-report"><span class="std std-ref">Reporting Bugs</span></a>.</p></li>
<li><p>If appropriate, provide URLs to background information or sage-devel
conversation relevant to the problem you are reporting.</p></li>
</ul>
<p><strong>When creating</strong> the ticket, you may find useful to read
<a class="reference internal" href="#section-trac-fields"><span class="std std-ref">The Ticket Fields</span></a>.</p>
<p>Unless you know what you are doing, leave the milestone field to its default
value.</p>
</section>
<section id="the-ticket-fields">
<span id="section-trac-fields"></span><h2>The Ticket Fields<a class="headerlink" href="#the-ticket-fields" title="Permalink to this headline">¶</a></h2>
<p>When you open a new ticket or change an existing ticket, you will find a variety
of fields that can be changed. Here is a comprehensive overview (for the
‘status’ field, see <a class="reference internal" href="#section-trac-ticket-status"><span class="std std-ref">The status of a ticket</span></a>):</p>
<ul class="simple">
<li><p><strong>Reported by:</strong> The trac account name of whoever created the
ticket. Cannot be changed.</p></li>
<li><p><strong>Owned by:</strong> Trac account name of owner, by default the person in charge of
the Component (see below). Generally not used in the Sage trac.</p></li>
<li><p><strong>Type:</strong> One of <code class="docutils literal notranslate"><span class="pre">enhancement</span></code> (e.g. a new feature), <code class="docutils literal notranslate"><span class="pre">defect</span></code> (e.g. a bug
fix), or <code class="docutils literal notranslate"><span class="pre">task</span></code> (rarely used).</p></li>
<li><p><strong>Priority:</strong> The priority of the ticket. Keep in mind that the
“blocker” label should be used very sparingly.</p></li>
<li><p><strong>Milestone:</strong> Milestones are usually goals to be met while working
toward a release. In Sage’s trac, we use milestones instead of
releases. Each ticket must have a milestone assigned. If you are
unsure, assign it to the current milestone.</p></li>
<li><p><strong>Component:</strong> A list of components of Sage, pick one that most
closely matches the ticket.</p></li>
<li><p><strong>Keywords:</strong> List of keywords. Fill in any keywords that you think
will make your ticket easier to find. Tickets that have been worked
on at Sage days <code class="docutils literal notranslate"><span class="pre">NN</span></code> (some number) ofter have <code class="docutils literal notranslate"><span class="pre">sdNN</span></code> as keyword.</p></li>
<li><p><strong>Cc:</strong> List of trac user names to Cc (send emails for changes on
the ticket). Note that users that enter a comment are automatically
substcribed to email updates and don’t need to be listed under Cc.</p></li>
<li><p><strong>Merged in:</strong> The Sage release where the ticket was merged in. Only
changed by the release manager.</p></li>
<li><p><strong>Authors:</strong> Real name of the ticket author(s).</p></li>
<li><p><strong>Reviewers:</strong> Real name of the ticket reviewer(s).</p></li>
<li><p><strong>Report Upstream:</strong> If the ticket is a bug in an upstream component
of Sage, this field is used to summarize the communication with the
upstream developers.</p></li>
<li><p><strong>Work issues:</strong> Issues that need to be resolved before the ticket
can leave the “needs work” status.</p></li>
<li><p><strong>Branch:</strong> The Git branch containing the ticket’s code (see
<a class="reference internal" href="walk_through.html#section-walkthrough-branch"><span class="std std-ref">Branching Out</span></a>). It is displayed in green color,
unless there is a conflict between the branch and the latest beta
release (red color). In this case, the branch should be merged or
rebased.</p></li>
<li><p><strong>Dependencies:</strong> Does the ticket depend on another ticket?
Sometimes, a ticket requires that another ticket be applied
first. If this is the case, put the dependencies as a
comma-separated list (<code class="docutils literal notranslate"><span class="pre">#1234,</span> <span class="pre">#5678</span></code>) into the “Dependencies:”
field.</p></li>
<li><p><strong>Stopgaps:</strong> See <a class="reference internal" href="#section-trac-stopgaps"><span class="std std-ref">Stopgaps</span></a>.</p></li>
</ul>
</section>
<section id="the-status-of-a-ticket">
<span id="section-trac-ticket-status"></span><h2>The status of a ticket<a class="headerlink" href="#the-status-of-a-ticket" title="Permalink to this headline">¶</a></h2>
<p>The status of a ticket appears right next to its number, at the top-left corner
of its page. It indicates who has to work on it.</p>
<ul>
<li><p><strong>new</strong> – the ticket has only been created (or the author forgot to change
the status to something else).</p>
<p>If you want to work on it yourself it is better to leave a comment to say
so. It could avoid having two persons doing the same job.</p>
</li>
<li><p><strong>needs_review</strong> – the code is ready to be peer-reviewed. If the code is not
yours, then you can review it. See <a class="reference internal" href="reviewer_checklist.html#chapter-review"><span class="std std-ref">The reviewer’s check list</span></a>.</p></li>
<li><p><strong>needs_work</strong> – something needs to be changed in the code. The reason should
appear in the comments.</p></li>
<li><p><strong>needs_info</strong> – somebody has to answer a question before anything else can
happen. It should be clear from the comments.</p></li>
<li><p><strong>positive_review</strong> – the ticket has been reviewed, and the release manager
will close it.</p></li>
</ul>
<p>The status of a ticket can be changed using a form at the bottom of the ticket’s
page. Leave a comment explaining your reasons whenever you change it.</p>
</section>
<section id="stopgaps">
<span id="section-trac-stopgaps"></span><h2>Stopgaps<a class="headerlink" href="#stopgaps" title="Permalink to this headline">¶</a></h2>
<p>When Sage returns wrong results, two tickets should be opened:</p>
<ul class="simple">
<li><p>A main ticket with all available details.</p></li>
<li><p>A “stopgap” ticket (e.g. <a class="reference external" href="https://trac.sagemath.org/12699">trac ticket #12699</a>)</p></li>
</ul>
<p>This second ticket does not fix the problem but adds a warning that will be
printed whenever anyone uses the relevant code. This, until the problem is
finally fixed.</p>
<p>To produce the warning message, use code like the following:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">from</span> <span class="nn">sage.misc.stopgap</span> <span class="kn">import</span> <span class="n">stopgap</span>
<span class="n">stopgap</span><span class="p">(</span><span class="s2">&quot;This code contains bugs and may be mathematically unreliable.&quot;</span><span class="p">,</span>
    <span class="n">TICKET_NUM</span><span class="p">)</span>
</pre></div>
</div>
<p>Replace <code class="docutils literal notranslate"><span class="pre">TICKET_NUM</span></code> by the ticket number for the main ticket. On the main
trac ticket, enter the ticket number for the stopgap ticket in the “Stopgaps”
field (see <a class="reference internal" href="#section-trac-fields"><span class="std std-ref">The Ticket Fields</span></a>). Stopgap tickets should be marked as
blockers.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>If mathematically valid code causes Sage to raise an error or
crash, for example, there is no need for a stopgap.  Rather,
stopgaps are to warn users that they may be using buggy code; if
Sage crashes, this is not an issue.</p>
</div>
</section>
<section id="working-on-tickets">
<h2>Working on Tickets<a class="headerlink" href="#working-on-tickets" title="Permalink to this headline">¶</a></h2>
<p>If you manage to fix a bug or enhance Sage you are our hero. See
<a class="reference internal" href="walk_through.html#chapter-walkthrough"><span class="std std-ref">Sage Development Process</span></a> for making changes to the Sage source
code, uploading them to the Sage trac server, and finally putting your
new branch on the trac ticket. The following are some other relevant
issues:</p>
<ul class="simple">
<li><p>The Patch buildbot will automatically test your ticket. See <a class="reference external" href="https://wiki.sagemath.org/buildbot">the
patchbot wiki</a> for more
information about its features and limitations. Make sure that you
look at the log, especially if the patch buildbot did not give you
the green blob.</p></li>
<li><p>Every bug fixed should result in a doctest.</p></li>
<li><p>This is not an issue with defects, but there are many enhancements
possible for Sage and too few developers to implement all the good
ideas. The trac server is useful for keeping ideas in a central
place because in the Google groups they tend to get lost once they
drop off the first page.</p></li>
<li><p>If you are a developer, be nice and try to solve a stale/old ticket
every once in a while.</p></li>
<li><p>Some people regularly do triage. In this context, this means that we
look at new bugs and classify them according to our perceived
priority. It is very likely that different people will see
priorities of bugs very differently from us, so please let us know
if you see a problem with specific tickets.</p></li>
</ul>
</section>
<section id="reviewing-and-closing-tickets">
<h2>Reviewing and closing Tickets<a class="headerlink" href="#reviewing-and-closing-tickets" title="Permalink to this headline">¶</a></h2>
<p>Tickets can be closed when they have positive review or for other reasons. To
learn how to review, please see <a class="reference internal" href="reviewer_checklist.html#chapter-review"><span class="std std-ref">The reviewer’s check list</span></a>.</p>
<p>Only the Sage release manager will close tickets. Most likely, this is
not you nor will your trac account have the necessary permissions. If
you feel strongly that a ticket should be closed or deleted, then
change the status of the ticket to <em>needs review</em> and change the
milestone to <em>sage-duplicate/invalid/wontfix</em>. You should also
comment on the ticket, explaining why it should be closed. If another
developer agrees, he sets the ticket to <em>positive review</em>.</p>
<p>A related issue is re-opening tickets. You should refrain from
re-opening a ticket that is already closed. Instead, open a new ticket
and provide a link in the description to the old ticket.</p>
</section>
<section id="reasons-to-invalidate-tickets">
<h2>Reasons to Invalidate Tickets<a class="headerlink" href="#reasons-to-invalidate-tickets" title="Permalink to this headline">¶</a></h2>
<p><strong>One Issue Per Ticket</strong>: A ticket must cover only one issue
and should not be a laundry list of unrelated issues. If a ticket
covers more than one issue, we cannot close it and while some of
the patches have been applied to a given release, the ticket would
remain in limbo.</p>
<p><strong>No Patch Bombs</strong>: Code that goes into Sage is peer-reviewed. If
you show up with an 80,000 lines of code bundle that completely
rips out a subsystem and replaces it with something else, you can
imagine that the review process will be a little tedious. These
huge patch bombs are problematic for several reasons and we prefer
small, gradual changes that are easy to review and apply. This is
not always possible (e.g. coercion rewrite), but it is still highly
recommended that you avoid this style of development unless there
is no way around it.</p>
<p><strong>Sage Specific</strong>: Sage’s philosophy is that we ship everything
(or close to it) in one source tarball to make debugging possible.
You can imagine the combinatorial explosion we would have to deal
with if you replaced only ten components of Sage with external
packages. Once you start replacing some of the more essential
components of Sage that are commonly packaged (e.g. Pari, GAP,
lisp, gmp), it is no longer a problem that belongs in our tracker.
If your distribution’s Pari package is buggy for example, file a
bug report with them. We are usually willing and able to solve
the problem, but there are no guarantees that we will help you
out. Looking at the open number of tickets that are Sage specific,
you hopefully will understand why.</p>
<p><strong>No Support Discussions</strong>: The trac installation is not meant to
be a system to track down problems when using Sage. Tickets should
be clearly a bug and not “I tried to do X and I couldn’t get it to
work. How do I do this?” That is usually not a bug in Sage and it
is likely that <code class="docutils literal notranslate"><span class="pre">sage-support</span></code> can answer that question for you. If
it turns out that you did hit a bug, somebody will open a concise
and to-the-point ticket.</p>
<p><strong>Solution Must Be Achievable</strong>: Tickets must be achievable. Many
times, tickets that fall into this category usually ran afoul to
some of the other rules listed above. An example would be to
“Make Sage the best CAS in the world”. There is no metric to
measure this properly and it is highly subjective.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">The Sage Trac Server</a><ul>
<li><a class="reference internal" href="#obtaining-an-account">Obtaining an Account</a></li>
<li><a class="reference internal" href="#trac-authentication-through-ssh">Trac authentication through SSH</a><ul>
<li><a class="reference internal" href="#generating-and-uploading-your-ssh-keys">Generating and Uploading your SSH Keys</a></li>
<li><a class="reference internal" href="#generating-your-ssh-keys">Generating your SSH Keys</a></li>
<li><a class="reference internal" href="#linking-your-public-key-to-your-trac-account">Linking your Public Key to your Trac Account</a></li>
</ul>
</li>
<li><a class="reference internal" href="#reporting-bugs">Reporting Bugs</a></li>
<li><a class="reference internal" href="#guidelines-for-opening-tickets">Guidelines for Opening Tickets</a></li>
<li><a class="reference internal" href="#the-ticket-fields">The Ticket Fields</a></li>
<li><a class="reference internal" href="#the-status-of-a-ticket">The status of a ticket</a></li>
<li><a class="reference internal" href="#stopgaps">Stopgaps</a></li>
<li><a class="reference internal" href="#working-on-tickets">Working on Tickets</a></li>
<li><a class="reference internal" href="#reviewing-and-closing-tickets">Reviewing and closing Tickets</a></li>
<li><a class="reference internal" href="#reasons-to-invalidate-tickets">Reasons to Invalidate Tickets</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="walk_through.html"
                          title="previous chapter">Sage Development Process</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="manual_git.html"
                          title="next chapter">Using Git with the Sage Trac Server</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/trac.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="manual_git.html" title="Using Git with the Sage Trac Server"
             >next</a> |</li>
        <li class="right" >
          <a href="walk_through.html" title="Sage Development Process"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="index.html">Developer&#39;s Guide</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">The Sage Trac Server</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>