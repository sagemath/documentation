
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Sage Development Process &#8212; Developer&#39;s Guide</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="_static/thebelab.css" />
    
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="The Sage Trac Server" href="trac.html" />
    <link rel="prev" title="Setting Up Git" href="git_setup.html" />
  <link rel="icon" href="_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="trac.html" title="The Sage Trac Server"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="git_setup.html" title="Setting Up Git"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="index.html">Developer&#39;s Guide</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Sage Development Process</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="sage-development-process">
<span id="chapter-walkthrough"></span><h1>Sage Development Process<a class="headerlink" href="#sage-development-process" title="Permalink to this headline">¶</a></h1>
<p>This section is a concise overview of the Sage development process. In
it, we will see how to make changes to the Sage source code and record
them in the <code class="docutils literal notranslate"><span class="pre">git</span></code> revision control system.</p>
<p>We also have a handy <a class="reference external" href="http://github.com/sagemath/git-trac-command/raw/master/doc/git-cheat-sheet.pdf">one-page “cheat sheet”</a>
of commonly used git commands that you can print out and leave on your
desk.  We have some <a class="reference internal" href="git_background.html#section-git-tutorials"><span class="std std-ref">recommended references and tutorials</span></a> as well.</p>
<p>In the following sections on <a class="reference internal" href="trac.html#chapter-sage-trac"><span class="std std-ref">The Sage Trac Server</span></a> and
<a class="reference internal" href="index.html#section-git-tricks-and-tips"><span class="std std-ref">Using Git with Trac</span></a> we will look at communicating these
changes back to the Sage project.  All changes to Sage source code
have to go through the <a class="reference external" href="https://trac.sagemath.org">Sage Trac development server</a>.</p>
<p>As an alternative to using the Trac server directly, you can fork and
create a Merge Request (MR) at <a class="reference external" href="https://gitlab.com/sagemath/sage">GitLab</a>
which will automatically fetch your code and open a ticket on our trac
server.</p>
<p>Pull Requests (PR) on GitHub are currently not supported by the
SageMath project.</p>
<section id="configuring-git">
<span id="section-walkthrough-setup-git"></span><h2>Configuring Git<a class="headerlink" href="#configuring-git" title="Permalink to this headline">¶</a></h2>
<p>One way or another, <code class="docutils literal notranslate"><span class="pre">git</span></code> is what Sage uses for tracking changes.
So first, open a shell (for instance, Terminal on Mac) and check that
<code class="docutils literal notranslate"><span class="pre">git</span></code> works:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost]$ </span>git
<span class="go">usage: git [--version] [--help] [-C &lt;path&gt;] [-c name=value]</span>
<span class="go">...</span>
<span class="go">The most commonly used git commands are:</span>
<span class="go">   add        Add file contents to the index</span>
<span class="go">...</span>
<span class="go">   tag        Create, list, delete or verify a tag object signed with GPG</span>

<span class="go">&#39;git help -a&#39; and &#39;git help -g&#39; lists available subcommands and some</span>
<span class="go">concept guides. See &#39;git help &lt;command&gt;&#39; or &#39;git help &lt;concept&gt;&#39;</span>
<span class="go">to read about a specific subcommand or concept.</span>
</pre></div>
</div>
<p>Don’t worry about the giant list of subcommands. You really only need
a handful for effective development, and we will walk you through them
in this guide. If you got a “command not found” error, then you don’t
have git installed. Now is the time to install it; see
<a class="reference internal" href="git_setup.html#chapter-git-setup"><span class="std std-ref">Setting Up Git</span></a> for instructions.</p>
<p>Because we also track who does changes in Sage with git, you must tell
git how you want to be known. This only needs to be done once:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost]$ </span>git config --global user.name <span class="s2">&quot;Your Name&quot;</span>
<span class="gp">[user@localhost]$ </span>git config --global user.email you@yourdomain.example.com
</pre></div>
</div>
<p>If you have multiple accounts / computers use the same name on each of
them. This name/email combination ends up in commits, so do it now
before you forget!</p>
</section>
<section id="obtaining-and-compiling-the-sage-source-code">
<span id="section-walkthrough-sage-source"></span><h2>Obtaining and Compiling the Sage Source Code<a class="headerlink" href="#obtaining-and-compiling-the-sage-source-code" title="Permalink to this headline">¶</a></h2>
<p>Obviously one needs the Sage source code to develop.  You can use your
local installation of Sage, or (to start from scratch) download it
from GitHub which is a public read-only mirror (=faster) of our
internal git repository:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost ~]$ </span>git clone https://github.com/sagemath/sage.git
<span class="go">Cloning into &#39;sage&#39;...</span>
<span class="go">[...]</span>
<span class="go">Checking connectivity... done.</span>
</pre></div>
</div>
<p>This creates a directory named <code class="docutils literal notranslate"><span class="pre">sage</span></code> containing the sources for the
current stable and development releases of Sage. You next need to switch
to the develop branch (latest development release):</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost ~]$ </span><span class="nb">cd</span> sage
<span class="gp">[user@localhost sage]$ </span>git checkout develop
</pre></div>
</div>
<p>Next, compile Sage, following the instruction in the file
<a class="reference external" href="https://github.com/sagemath/sage/#readme">README.md</a> in <code class="docutils literal notranslate"><span class="pre">SAGE_ROOT</span></code>.
Additional details can be found in the
<a class="reference external" href="../installation/source.html">section on installation from source</a>
in the Sage installation guide.
If you wish to use conda-forge, see the <a class="reference external" href="../installation/conda.html">section on conda</a>.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>macOS allows changing directories without using exact capitalization.
Beware of this convenience when compiling for macOS. Ignoring exact
capitalization when changing into <span class="target" id="index-0"></span><code class="xref std std-envvar docutils literal notranslate"><span class="pre">SAGE_ROOT</span></code> can lead to build
errors for dependencies requiring exact capitalization in path names.</p>
</div>
<p>For the experts, note that the repository at
<a class="reference external" href="http://git.sagemath.org">git.sagemath.org</a> is where development
actually takes place.</p>
</section>
<section id="branching-out">
<span id="section-walkthrough-branch"></span><h2>Branching Out<a class="headerlink" href="#branching-out" title="Permalink to this headline">¶</a></h2>
<p>In order to start modifying Sage, we want to make a <em>branch</em> of Sage.
A branch is a copy (except that it doesn’t take up twice the space) of
the Sage source code where you can store your modifications to the
Sage source code and which you can upload to trac tickets.</p>
<p>To begin with, type the command <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">branch</span></code>. You will see the following:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git branch
<span class="go">* develop</span>
<span class="go">  master</span>
</pre></div>
</div>
<p>The asterisk shows you which branch you are on. Without an argument,
the <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">branch</span></code> command displays a list of all local branches
with the current one marked by an asterisk.</p>
<p>It is easy to create a new branch; first make sure you are on the branch from
which you want to branch out. That is, if you are not currently on the
<code class="docutils literal notranslate"><span class="pre">develop</span></code> branch, type the command <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">checkout</span> <span class="pre">develop</span></code>:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git checkout develop
<span class="go">Switched to branch &#39;develop&#39;</span>
<span class="go">Your branch is up-to-date with &#39;origin/develop&#39;.</span>
</pre></div>
</div>
<p>Then use the <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">branch</span></code> command to create a new branch, as follows:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git branch last_twin_prime
</pre></div>
</div>
<p>Also note that <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">branch</span></code> creates a new branch, but does not switch
to it. For this, you have to use <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">checkout</span></code>:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git checkout last_twin_prime
<span class="go">Switched to branch &#39;last_twin_prime&#39;</span>
</pre></div>
</div>
<p>Now if you use the command <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">branch</span></code>, you will see the following:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git branch
<span class="go">  develop</span>
<span class="go">* last_twin_prime</span>
<span class="go">  master</span>
</pre></div>
</div>
<p>Note that unless you explicitly upload (“push”) a branch to a remote
git repository, the branch is a local branch that is only on your computer
and not visible to anyone else.</p>
<p>To avoid typing the new branch name twice you can use the shortcut
<code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">checkout</span> <span class="pre">-b</span> <span class="pre">my_new_branch</span></code> to create and switch to the new
branch in one command.</p>
</section>
<section id="the-history">
<span id="section-walkthrough-logs"></span><h2>The History<a class="headerlink" href="#the-history" title="Permalink to this headline">¶</a></h2>
<p>It is always a good idea to check that you are making your edits on
the version that you think you are on. The first one shows you the
topmost commit in detail, including its changes to the sources:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git show
</pre></div>
</div>
<p>To dig deeper, you can inspect the log:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git log
</pre></div>
</div>
<p>By default, this lists all commits in reverse chronological order.</p>
<ul class="simple">
<li><p>If you find your branch to be in the wrong place, see the
<a class="reference internal" href="advanced_git.html#section-git-recovery"><span class="std std-ref">Reset and Recovery</span></a> section.</p></li>
<li><p>Many programs are available to help you visualize the history tree
better. <code class="docutils literal notranslate"><span class="pre">tig</span></code> is a very nice text-mode such tool.</p></li>
</ul>
</section>
<section id="editing-the-source-code">
<span id="section-walkthrough-add-edit"></span><h2>Editing the Source Code<a class="headerlink" href="#editing-the-source-code" title="Permalink to this headline">¶</a></h2>
<p>Once you have your own branch, feel free to make any changes as you
like. <a class="reference internal" href="index.html#section-writing-code-for-sage"><span class="std std-ref">Subsequent chapters</span></a> of
this developer guide explain how your code should look like to fit
into Sage, and how we ensure high code quality throughout.</p>
<p><em>Status</em> is probably the most important git command. It tells
you which files changed, and how to continue with recording the
changes:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git status
<span class="go">On branch last_twin_prime</span>
<span class="go">Changes not staged for commit:</span>
<span class="go">  (use &quot;git add &lt;file&gt;...&quot; to update what will be committed)</span>
<span class="go">  (use &quot;git checkout -- &lt;file&gt;...&quot; to discard changes in working directory)</span>

<span class="go">    modified:   some_file.py</span>
<span class="go">    modified:   src/sage/primes/all.py</span>

<span class="go">Untracked files:</span>
<span class="go">  (use &quot;git add &lt;file&gt;...&quot; to include in what will be committed)</span>

<span class="go">    src/sage/primes/last_pair.py</span>

<span class="go">no changes added to commit (use &quot;git add&quot; and/or &quot;git commit -a&quot;)</span>
</pre></div>
</div>
<p>To dig deeper into what was changed in the files you can use:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git diff some_file.py
</pre></div>
</div>
<p>to show you the differences.</p>
</section>
<section id="rebuilding-sage">
<span id="section-walkthrough-make"></span><h2>Rebuilding Sage<a class="headerlink" href="#rebuilding-sage" title="Permalink to this headline">¶</a></h2>
<p>Once you have made any changes you of course want to build Sage and
try out your edits. As long as you only modified the Sage library
(that is, Python and Cython files under <code class="docutils literal notranslate"><span class="pre">src/sage/...</span></code>) you just
have to run:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>./sage -br
</pre></div>
</div>
<p>to rebuild the Sage library and then start Sage. This should be quite
fast. If you made changes to
<a class="reference internal" href="packaging.html#chapter-packaging"><span class="std std-ref">third-party packages</span></a>, then you have to run</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>make build
</pre></div>
</div>
<p>as if you were <a class="reference external" href="http://doc.sagemath.org/html/en/installation/source.html">installing Sage from scratch</a>.
However, this time only packages which were changed (or which depend
on a changed package) will be recompiled,
so it should be much faster than compiling Sage
the first time.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>If you have <a class="reference external" href="http://doc.sagemath.org/html/en/developer/manual_git.html#checking-out-tickets">pulled a branch from trac</a>,
it may depend on changes to third-party packages, so <code class="docutils literal notranslate"><span class="pre">./sage</span> <span class="pre">-br</span></code>
may fail.  If this happens (and you believe the code in this branch
should compile), try running <code class="docutils literal notranslate"><span class="pre">make</span> <span class="pre">build</span></code>.</p>
</div>
<p>Rarely there are conflicts with other packages,
or with the already-installed older version of the package that you
changed, in that case you do have to recompile everything using:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>make distclean <span class="o">&amp;&amp;</span> make build
</pre></div>
</div>
<p>Also, don’t forget to run the tests (see <a class="reference internal" href="doctesting.html#chapter-doctesting"><span class="std std-ref">Running Sage’s doctests</span></a>)
and build the documentation (see <a class="reference internal" href="sage_manuals.html#chapter-sage-manuals"><span class="std std-ref">The Sage Manuals</span></a>).</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>If you switch between branches based on different releases, the timestamps
of modified files will change. This triggers recythonization and recompilation
of modified files on subsequent builds, whether or not you have made any
additional changes to files. To minimize the impact of switching between branches,
install ccache using the command</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>./sage -i ccache
</pre></div>
</div>
<p>Recythonization will still occur when rebuilding, but the recompilation stage
first checks whether previously compiled files are cached for reuse before
compiling them again. This saves considerable time rebuilding.</p>
</div>
</section>
<section id="commits-snapshots">
<span id="section-walkthrough-commit"></span><h2>Commits (Snapshots)<a class="headerlink" href="#commits-snapshots" title="Permalink to this headline">¶</a></h2>
<p>Whenever you have reached your goal, a milestone towards it, or
just feel like you got some work done you should <em>commit</em> your
changes. A commit is just a snapshot of the state of all files in
the <em>repository</em> (the program you are working on).</p>
<p>Unlike with some other revision control programs, in git you first
need to <em>stage</em> the changed files, which tells git which files you
want to be part of the next commit:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git status
<span class="gp"># </span>On branch my_branch
<span class="gp"># </span>Untracked files:
<span class="gp">#   </span><span class="o">(</span>use <span class="s2">&quot;git add &lt;file&gt;...&quot;</span> to include <span class="k">in</span> what will be committed<span class="o">)</span>
<span class="gp">#</span>
<span class="gp">#       </span>src/sage/primes/last_pair.py
<span class="go">nothing added to commit but untracked files present (use &quot;git add&quot; to track)</span>

<span class="gp">[user@localhost sage]$ </span>git add src/sage/primes/last_pair.py
<span class="gp">[user@localhost sage]$ </span>git status
<span class="gp"># </span>On branch my_branch
<span class="gp"># </span>Changes to be committed:
<span class="gp">#   </span><span class="o">(</span>use <span class="s2">&quot;git reset HEAD &lt;file&gt;...&quot;</span> to unstage<span class="o">)</span>
<span class="gp">#</span>
<span class="gp">#   </span>new file:   src/sage/primes/last_pair.py
<span class="gp">#</span>
</pre></div>
</div>
<p>Once you are satisfied with the list of staged files, you create a new
snapshot with the <code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">commit</span></code> command:</p>
<div class="highlight-shell-session notranslate"><div class="highlight"><pre><span></span><span class="gp">[user@localhost sage]$ </span>git commit
<span class="go">... editor opens ...</span>
<span class="go">[my_branch 31331f7] Added the very important foobar text file</span>
<span class="go"> 1 file changed, 1 insertion(+)</span>
<span class="go">  create mode 100644 foobar.txt</span>
</pre></div>
</div>
<p>This will open an editor for you to write your commit message. The
commit message should generally have a one-line description, followed
by an empty line, followed by further explanatory text:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>Added the last twin prime

This is an example commit message. You see there is a one-line
summary followed by more detailed description, if necessary.
</pre></div>
</div>
<p>You can then continue working towards your next milestone, make
another commit, repeat until finished. As long as you do not
<code class="docutils literal notranslate"><span class="pre">git</span> <span class="pre">checkout</span></code> another branch, all commits that you make will be part of
the branch that you created.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Sage Development Process</a><ul>
<li><a class="reference internal" href="#configuring-git">Configuring Git</a></li>
<li><a class="reference internal" href="#obtaining-and-compiling-the-sage-source-code">Obtaining and Compiling the Sage Source Code</a></li>
<li><a class="reference internal" href="#branching-out">Branching Out</a></li>
<li><a class="reference internal" href="#the-history">The History</a></li>
<li><a class="reference internal" href="#editing-the-source-code">Editing the Source Code</a></li>
<li><a class="reference internal" href="#rebuilding-sage">Rebuilding Sage</a></li>
<li><a class="reference internal" href="#commits-snapshots">Commits (Snapshots)</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="git_setup.html"
                          title="previous chapter">Setting Up Git</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="trac.html"
                          title="next chapter">The Sage Trac Server</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/walk_through.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="trac.html" title="The Sage Trac Server"
             >next</a> |</li>
        <li class="right" >
          <a href="git_setup.html" title="Setting Up Git"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="index.html">Developer&#39;s Guide</a> &#187;
    
  </li>

        <li class="nav-item nav-item-this"><a href="">Sage Development Process</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2005--2022, The Sage Development Team.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>