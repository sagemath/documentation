
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=600, initial-scale=1">
    <title>Sage Quickstart for Number Theory &#8212; PREP Tutorials</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/sage.css" />
    <link rel="stylesheet" type="text/css" href="../_static/plot_directive.css" />
    <link rel="stylesheet" type="text/css" href="../_static/jupyter-sphinx.css" />
    <link rel="stylesheet" type="text/css" href="../_static/thebelab.css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script src="../_static/thebelab-helper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"></script>
    <script src="https://unpkg.com/@jupyter-widgets/html-manager@^0.20.0/dist/embed-amd.js"></script>
    <script async="async" src="../_static/MathJax.js?config=TeX-AMS_HTML-full,../mathjax_sage.js"></script>
    
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Sage Quickstart for Statistics" href="Statistics-and-Distributions.html" />
    <link rel="prev" title="Sage Quickstart for Numerical Analysis" href="NumAnalysis.html" />
  <link rel="icon" href="../_static/sageicon.png" type="image/x-icon" />
  <!-- <script src="../_static/thebe.js" type="text/javascript"></script> -->
  <!-- <script src="../_static/thebe-sage.js" type="text/javascript"></script> -->

  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="Statistics-and-Distributions.html" title="Sage Quickstart for Statistics"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="NumAnalysis.html" title="Sage Quickstart for Numerical Analysis"
             accesskey="P">previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../index.html">PREP Tutorials</a> &#187;
    
  </li>

          <li class="nav-item nav-item-1"><a href="../quickstart.html" accesskey="U">PREP Quickstart Tutorials</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Sage Quickstart for Number Theory</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="sage-quickstart-for-number-theory">
<span id="prep-quickstart-number-theory"></span><h1>Sage Quickstart for Number Theory<a class="headerlink" href="#sage-quickstart-for-number-theory" title="Permalink to this headline">¶</a></h1>
<p>This <a class="reference external" href="http://www.sagemath.org">Sage</a> quickstart tutorial was developed
for the MAA PREP Workshop “Sage: Using Open-Source Mathematics Software
with Undergraduates” (funding provided by NSF DUE 0817071).  It is
licensed under the Creative Commons Attribution-ShareAlike 3.0 license
(<a class="reference external" href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA</a>).</p>
<p>Since Sage began life as a project in algebraic and analytic number
theory (and this continues to be a big emphasis), it is no surprise that
functionality in this area is extremely comprehensive.  And it’s also
just enjoyable to explore elementary number theory by computer.</p>
<section id="modular-arithmetic">
<h2>Modular Arithmetic<a class="headerlink" href="#modular-arithmetic" title="Permalink to this headline">¶</a></h2>
<p>Conveniently, the ring of integers
modulo <span class="math notranslate nohighlight">\(n\)</span> is always available in Sage, so we can do
modular arithmetic very easily.
For instance, we can create a number in <span class="math notranslate nohighlight">\(\ZZ/11\ZZ\)</span>.  The
<code class="docutils literal notranslate"><span class="pre">type</span></code> command tells us that <span class="math notranslate nohighlight">\(a\)</span> is not a regular integer.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">a</span> <span class="o">=</span> <span class="n">mod</span><span class="p">(</span><span class="mi">2</span><span class="p">,</span><span class="mi">11</span><span class="p">);</span> <span class="n">a</span><span class="p">;</span> <span class="nb">type</span><span class="p">(</span><span class="n">a</span><span class="p">);</span> <span class="n">a</span><span class="o">^</span><span class="mi">10</span><span class="p">;</span> <span class="n">a</span><span class="o">^</span><span class="mi">1000000</span>
<span class="go">2</span>
<span class="go">&lt;class &#39;sage.rings.finite_rings.integer_mod.IntegerMod_int&#39;&gt;</span>
<span class="go">1</span>
<span class="go">1</span>
</pre></div>
</div>
<p>Note that we verified Fermat’s “Little Theorem” in the previous cell,
for the prime <span class="math notranslate nohighlight">\(p=11\)</span> and input <span class="math notranslate nohighlight">\(a=2\)</span>.</p>
<p>Recalling the basic programming construct called a <em>loop</em> , we can
verify this for all <span class="math notranslate nohighlight">\(a\)</span> in the integers modulo <span class="math notranslate nohighlight">\(p\)</span>.  Here,
instead of looping over an explicit list like <code class="docutils literal notranslate"><span class="pre">[0,1,2,3,...8,9,10]</span></code>,
we loop over a Sage object.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="k">for</span> <span class="n">a</span> <span class="ow">in</span> <span class="n">Integers</span><span class="p">(</span><span class="mi">11</span><span class="p">):</span>
<span class="gp">....: </span>    <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;</span><span class="si">{}</span><span class="s2"> </span><span class="si">{}</span><span class="s2">&quot;</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">a</span><span class="p">,</span> <span class="n">a</span><span class="o">^</span><span class="mi">10</span><span class="p">))</span>
<span class="go">0 0</span>
<span class="go">1 1</span>
<span class="go">2 1</span>
<span class="go">3 1</span>
<span class="go">4 1</span>
<span class="go">5 1</span>
<span class="go">6 1</span>
<span class="go">7 1</span>
<span class="go">8 1</span>
<span class="go">9 1</span>
<span class="go">10 1</span>
</pre></div>
</div>
<p>Notice that <code class="docutils literal notranslate"><span class="pre">Integers(11)</span></code> gave us an algebraic object which is the
ring of integers modulo the prime ideal generated by the element 11.</p>
<p>This works for much bigger numbers too, of course.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">p</span><span class="o">=</span><span class="n">random_prime</span><span class="p">(</span><span class="mi">10</span><span class="o">^</span><span class="mi">200</span><span class="p">,</span><span class="n">proof</span><span class="o">=</span><span class="kc">True</span><span class="p">)</span>
<span class="gp">sage: </span><span class="n">Zp</span><span class="o">=</span><span class="n">Integers</span><span class="p">(</span><span class="n">p</span><span class="p">)</span> <span class="c1"># Here we give ourselves shorthand for the modular integers</span>
<span class="gp">sage: </span><span class="n">a</span><span class="o">=</span><span class="n">Zp</span><span class="p">(</span><span class="mi">2</span><span class="p">)</span> <span class="c1"># Here we ask for 2 as an element of that ring</span>
<span class="gp">sage: </span><span class="n">p</span><span class="p">;</span> <span class="n">a</span><span class="p">;</span> <span class="n">a</span><span class="o">^</span><span class="p">(</span><span class="n">p</span><span class="o">-</span><span class="mi">1</span><span class="p">);</span> <span class="n">a</span><span class="o">^</span><span class="p">(</span><span class="mi">10</span><span class="o">^</span><span class="mi">400</span><span class="p">)</span>
<span class="go">83127880126958116183078749835647757239842289608629126718968330784897646881689610705533628095938824110164366160161355539845499311180100402016248362566463907409939681883876411550651284088712896660589151</span>
<span class="go">2</span>
<span class="go">1</span>
<span class="go">21428062940380156097538386722913390559445420264965409656232714664156136144920173818936415427844953758774658350253363113516712541610660591925149144205368271119123211091215746697984955519927521190733305</span>
</pre></div>
</div>
<p>Whenever you encounter a new object, you should try tab-completion to
see what you can do to it.  Try it here, and pick one (hopefully one
that won’t be too long!).</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">Zp</span><span class="o">.</span>
</pre></div>
</div>
<p>Here’s one that sounds interesting.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span>Zp.zeta<span class="o">?</span>
</pre></div>
</div>
<p>And we use it to find the fifth roots of unity in this field.  We use
the <em>list comprehension</em> (set-builder) notation from the programming
tutorial this time.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">root_list</span> <span class="o">=</span> <span class="n">Zp</span><span class="o">.</span><span class="n">zeta</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span><span class="nb">all</span><span class="o">=</span><span class="kc">True</span><span class="p">);</span> <span class="n">root_list</span>
<span class="go">[80199770388563324100334548626345240081294273289109866436996652525328268652922508892946068538641538316054373187019168781211876036849531337824832226216684677717580165592175377569174402189281574130719978, 69839783895572286297568834485025073557885364348071061715465477061873400359794989367423407683971299361817245213947182344090739843367076197016322541936552333837227080274674865687645877633828974738751695, 57407444219199061498801298672323590163238592201574572482836619025676869537007609315386800852204337587805249250896651467970585450518157701115893749407382500580682168292929753154872678880962261809848942, 41936641877539676652531567723249367917108638987131879521606243741814402095343724540844607212999297064816230828621064026263296602805535970091696570138772210094329631491849238240260893562065879302446837]</span>
</pre></div>
</div>
<p>Are these really fifth roots of unity?</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="p">[</span><span class="n">root</span><span class="o">^</span><span class="mi">5</span> <span class="k">for</span> <span class="n">root</span> <span class="ow">in</span> <span class="n">root_list</span><span class="p">]</span>
<span class="go">[1, 1, 1, 1]</span>
</pre></div>
</div>
<p>Luckily, it checked out.
(If you didn’t get any, then your random prime ended up being one for
which there <em>are</em> no fifth roots of unity – try doing the sequence
over again!)</p>
</section>
<section id="more-basic-functionality">
<h2>More basic functionality<a class="headerlink" href="#more-basic-functionality" title="Permalink to this headline">¶</a></h2>
<p>Similarly to the situation in linear algebra, there is much more we can
access at the elementary level, such as</p>
<ul class="simple">
<li><p>primitive roots,</p></li>
<li><p>ways to write a number as a sum of squares,</p></li>
<li><p>Legendre symbols,</p></li>
<li><p>modular solving of basic equations,</p></li>
<li><p>etc.</p></li>
</ul>
<p>A good way to use Sage in this context is to allow students to
experiment with pencil and paper first, then use Sage to see whether
patterns they discover hold true before attempting to prove them.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">p</span> <span class="o">=</span> <span class="mi">13</span>
<span class="gp">sage: </span><span class="n">primitive_root</span><span class="p">(</span><span class="n">p</span><span class="p">);</span> <span class="n">two_squares</span><span class="p">(</span><span class="n">p</span><span class="p">);</span> <span class="n">is_prime</span><span class="p">(</span><span class="n">p</span><span class="p">)</span>
<span class="go">2</span>
<span class="go">(2, 3)</span>
<span class="go">True</span>
</pre></div>
</div>
<p>This makes it easy to construct elementary cryptographic examples as
well.  Here is a standard example of a Diffie-Hellman key exchange, for
instance.  If we didn’t do the second line, exponentiation would be
impractical.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">p</span><span class="o">=</span><span class="n">random_prime</span><span class="p">(</span><span class="mi">10</span><span class="o">^</span><span class="mi">20</span><span class="p">,</span><span class="mi">10</span><span class="o">^</span><span class="mi">30</span><span class="p">)</span> <span class="c1"># a random prime between these numbers</span>
<span class="gp">sage: </span><span class="n">q</span><span class="o">=</span><span class="n">mod</span><span class="p">(</span><span class="n">primitive_root</span><span class="p">(</span><span class="n">p</span><span class="p">),</span><span class="n">p</span><span class="p">)</span> <span class="c1"># makes the primitive root a number modulo p, not an integer</span>
<span class="gp">sage: </span><span class="n">n</span><span class="o">=</span><span class="n">randint</span><span class="p">(</span><span class="mi">1</span><span class="p">,</span><span class="n">p</span><span class="p">)</span> <span class="c1"># Alice&#39;s random number</span>
<span class="gp">sage: </span><span class="n">m</span><span class="o">=</span><span class="n">randint</span><span class="p">(</span><span class="mi">1</span><span class="p">,</span><span class="n">p</span><span class="p">)</span> <span class="c1"># Bob&#39;s random number</span>
<span class="gp">sage: </span><span class="n">x</span><span class="o">=</span><span class="n">q</span><span class="o">^</span><span class="n">n</span><span class="p">;</span> <span class="n">y</span><span class="o">=</span><span class="n">q</span><span class="o">^</span><span class="n">m</span>
<span class="gp">sage: </span><span class="n">x</span><span class="p">;</span> <span class="n">y</span><span class="p">;</span> <span class="n">x</span><span class="o">^</span><span class="n">m</span><span class="p">;</span> <span class="n">y</span><span class="o">^</span><span class="n">n</span>
<span class="go">66786436189350477660</span>
<span class="go">77232558812003408270</span>
<span class="go">45432410008036883324</span>
<span class="go">45432410008036883324</span>
</pre></div>
</div>
<p>The final line of the cell first requests Alice and Bob’s (possibly)
public information, and then verifies that the private keys they get are
the same.</p>
<p>It is hard to resist including just one interact.  How many theorems do
you see here?</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="nd">@interact</span>
<span class="gp">sage: </span><span class="k">def</span> <span class="nf">power_table_plot</span><span class="p">(</span><span class="n">p</span><span class="o">=</span><span class="p">(</span><span class="mi">7</span><span class="p">,</span><span class="n">prime_range</span><span class="p">(</span><span class="mi">50</span><span class="p">))):</span>
<span class="gp">....: </span>    <span class="n">P</span><span class="o">=</span><span class="n">matrix_plot</span><span class="p">(</span><span class="n">matrix</span><span class="p">(</span><span class="n">p</span><span class="o">-</span><span class="mi">1</span><span class="p">,[</span><span class="n">mod</span><span class="p">(</span><span class="n">a</span><span class="p">,</span><span class="n">p</span><span class="p">)</span><span class="o">^</span><span class="n">b</span> <span class="k">for</span> <span class="n">a</span> <span class="ow">in</span> <span class="nb">range</span><span class="p">(</span><span class="mi">1</span><span class="p">,</span><span class="n">p</span><span class="p">)</span> <span class="k">for</span> <span class="n">b</span> <span class="ow">in</span> <span class="n">srange</span><span class="p">(</span><span class="n">p</span><span class="p">)]),</span><span class="n">cmap</span><span class="o">=</span><span class="s1">&#39;jet&#39;</span><span class="p">)</span>
<span class="gp">....: </span>    <span class="n">show</span><span class="p">(</span><span class="n">P</span><span class="p">)</span>
</pre></div>
</div>
<p>This is a graphic giving the various powers of integers modulo <span class="math notranslate nohighlight">\(p\)</span>
as colors, not numbers.  The columns are the powers, so the first column
is the zeroth power (always 1) and the second column gives the colors
for the numbers modulo the given prime (first power).</p>
<p>One more very useful object is the prime counting function
<span class="math notranslate nohighlight">\(\pi(x)\)</span>.  This comes with its own custom plotting.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">prime_pi</span><span class="p">(</span><span class="mi">100</span><span class="p">);</span> <span class="n">plot</span><span class="p">(</span><span class="n">prime_pi</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">100</span><span class="p">)</span>
<span class="go">25</span>
<span class="go">Graphics object consisting of 1 graphics primitive</span>
</pre></div>
</div>
<p>A very nice aspect of Sage is combining several aspects of mathematics
together.  It can be very eye-opening to students to see analytic
aspects of number theory early on.
(Note that we have to reassign <span class="math notranslate nohighlight">\(x\)</span> to a variable, since above it
was a cryptographic key!)</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">var</span><span class="p">(</span><span class="s1">&#39;x&#39;</span><span class="p">)</span>
<span class="go">x</span>
<span class="gp">sage: </span><span class="n">plot</span><span class="p">(</span><span class="n">prime_pi</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">10</span><span class="o">^</span><span class="mi">6</span><span class="p">,</span><span class="n">thickness</span><span class="o">=</span><span class="mi">2</span><span class="p">)</span><span class="o">+</span><span class="n">plot</span><span class="p">(</span><span class="n">Li</span><span class="p">,</span><span class="mi">2</span><span class="p">,</span><span class="mi">10</span><span class="o">^</span><span class="mi">6</span><span class="p">,</span><span class="n">color</span><span class="o">=</span><span class="s1">&#39;red&#39;</span><span class="p">)</span><span class="o">+</span><span class="n">plot</span><span class="p">(</span><span class="n">x</span><span class="o">/</span><span class="n">ln</span><span class="p">(</span><span class="n">x</span><span class="p">),</span><span class="mi">2</span><span class="p">,</span><span class="mi">10</span><span class="o">^</span><span class="mi">6</span><span class="p">,</span><span class="n">color</span><span class="o">=</span><span class="s1">&#39;green&#39;</span><span class="p">)</span>
<span class="go">Graphics object consisting of 3 graphics primitives</span>
</pre></div>
</div>
</section>
<section id="advanced-number-theory">
<h2>Advanced Number Theory<a class="headerlink" href="#advanced-number-theory" title="Permalink to this headline">¶</a></h2>
<p>For those who are interested, more advanced number-theoretic objects
are easy to come by; we end with a brief sampler of these.</p>
<p>In the first example, <span class="math notranslate nohighlight">\(K\)</span> is the field extension
<span class="math notranslate nohighlight">\(\QQ(\sqrt{-14})\)</span>, where the symbol <code class="docutils literal notranslate"><span class="pre">a</span></code> plays the role of
<span class="math notranslate nohighlight">\(\sqrt{-14}\)</span>; we discover several basic facts about <span class="math notranslate nohighlight">\(K\)</span> in
the next several cells.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">K</span><span class="o">.&lt;</span><span class="n">a</span><span class="o">&gt;</span> <span class="o">=</span> <span class="n">NumberField</span><span class="p">(</span><span class="n">x</span><span class="o">^</span><span class="mi">2</span><span class="o">+</span><span class="mi">14</span><span class="p">);</span> <span class="n">K</span>
<span class="go">Number Field in a with defining polynomial x^2 + 14</span>
</pre></div>
</div>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">K</span><span class="o">.</span><span class="n">discriminant</span><span class="p">();</span> <span class="n">K</span><span class="o">.</span><span class="n">class_group</span><span class="p">()</span><span class="o">.</span><span class="n">order</span><span class="p">();</span> <span class="n">K</span><span class="o">.</span><span class="n">class_group</span><span class="p">()</span><span class="o">.</span><span class="n">is_cyclic</span><span class="p">()</span>
<span class="go">-56</span>
<span class="go">4</span>
<span class="go">True</span>
</pre></div>
</div>
<p>Various zeta functions are also available; here is a complex plot of the
Riemann zeta.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="n">complex_plot</span><span class="p">(</span><span class="n">zeta</span><span class="p">,</span> <span class="p">(</span><span class="o">-</span><span class="mi">30</span><span class="p">,</span><span class="mi">30</span><span class="p">),</span> <span class="p">(</span><span class="o">-</span><span class="mi">30</span><span class="p">,</span><span class="mi">30</span><span class="p">))</span>
<span class="go">Graphics object consisting of 1 graphics primitive</span>
</pre></div>
</div>
</section>
<section id="cryptography">
<h2>Cryptography<a class="headerlink" href="#cryptography" title="Permalink to this headline">¶</a></h2>
<p>Sage supports various more advanced cryptographic procedures as well as
some basic pedagogical ones natively.  This example is adapted from the
documentation.</p>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="kn">from</span> <span class="nn">sage.crypto.block_cipher.sdes</span> <span class="kn">import</span> <span class="n">SimplifiedDES</span>
<span class="gp">sage: </span><span class="n">sdes</span> <span class="o">=</span> <span class="n">SimplifiedDES</span><span class="p">();</span> <span class="n">sdes</span>
<span class="go">Simplified DES block cipher with 10-bit keys</span>
</pre></div>
</div>
<div class="highlight-ipycon notranslate"><div class="highlight"><pre><span></span><span class="gp">sage: </span><span class="nb">bin</span> <span class="o">=</span> <span class="n">BinaryStrings</span><span class="p">()</span>
<span class="gp">sage: </span><span class="n">P</span> <span class="o">=</span> <span class="p">[</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">1</span><span class="p">]</span> <span class="c1"># our message</span>
<span class="gp">sage: </span><span class="n">K</span> <span class="o">=</span> <span class="n">sdes</span><span class="o">.</span><span class="n">random_key</span><span class="p">()</span> <span class="c1"># generate a random key</span>
<span class="gp">sage: </span><span class="n">C</span> <span class="o">=</span> <span class="n">sdes</span><span class="o">.</span><span class="n">encrypt</span><span class="p">(</span><span class="n">P</span><span class="p">,</span> <span class="n">K</span><span class="p">)</span> <span class="c1"># encrypt our message</span>
<span class="gp">sage: </span><span class="n">plaintxt</span> <span class="o">=</span> <span class="n">sdes</span><span class="o">.</span><span class="n">decrypt</span><span class="p">(</span><span class="n">C</span><span class="p">,</span> <span class="n">K</span><span class="p">)</span> <span class="c1"># decrypt it</span>
<span class="gp">sage: </span><span class="n">plaintxt</span> <span class="c1"># print it</span>
<span class="go">[0, 1, 0, 0, 1, 1, 0, 1]</span>
</pre></div>
</div>
<p>See also the cryptography example in the <a class="reference internal" href="Graphs-and-Discrete.html#cryptoed"><span class="std std-ref">discrete math quickstart</span></a>.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="../index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Sage Quickstart for Number Theory</a><ul>
<li><a class="reference internal" href="#modular-arithmetic">Modular Arithmetic</a></li>
<li><a class="reference internal" href="#more-basic-functionality">More basic functionality</a></li>
<li><a class="reference internal" href="#advanced-number-theory">Advanced Number Theory</a></li>
<li><a class="reference internal" href="#cryptography">Cryptography</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="NumAnalysis.html"
                          title="previous chapter">Sage Quickstart for Numerical Analysis</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="Statistics-and-Distributions.html"
                          title="next chapter">Sage Quickstart for Statistics</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/Quickstarts/Number-Theory.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="Statistics-and-Distributions.html" title="Sage Quickstart for Statistics"
             >next</a> |</li>
        <li class="right" >
          <a href="NumAnalysis.html" title="Sage Quickstart for Numerical Analysis"
             >previous</a> |</li>
  <li class="nav-item nav-item-0">
    <a href="http://www.sagemath.org"><img src="../_static/logo_sagemath_black.svg" class="sage-logo" title="Sage Logo"></a>
    
      <a href="../../index.html">Sage 9.6 Documentation</a> &#187;
      
      <a href="../index.html">PREP Tutorials</a> &#187;
    
  </li>

          <li class="nav-item nav-item-1"><a href="../quickstart.html" >PREP Quickstart Tutorials</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Sage Quickstart for Number Theory</a></li> 
      </ul>
    </div>
  
    <div class="footer" role="contentinfo">
        &#169; Copyright 2012, Rob Beezer, Karl-Dieter Crisman, and Jason Grout.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  <script type="text/javascript">
/*global jQuery, window */
/* Sphinx sidebar toggle.  Putting this code at the end of the body
 * enables the toggle for the live, static, and offline docs.  Note:
 * sage.misc.html.math_parse() eats jQuery's dollar-sign shortcut. */
var jq = jQuery;
jq(document).ready(function () {
    var bar, bod, bg, fg, key, tog, wid_old, wid_new, get_state, set_state;
    bod = jq('div.bodywrapper');
    bar = jq('div.sphinxsidebar');
    tog = jq('<div class="sphinxsidebartoggle"></div>');

    /* The sidebar toggle adapts its height to the bodywrapper height. */
    const resizeObserver = new ResizeObserver(entries => {
        tog.height(bod.height());
    });
    resizeObserver.observe(bod[0]);

    /* Setup and add the toggle. See Sphinx v0.5.1 default.css. */
    fg = jq('div.sphinxsidebar p a').css('color') || 'rgb(152, 219, 204)';
    bg = jq('div.document').css('background-color') || 'rgb(28, 78, 99)';
    wid_old = '230px';
    wid_new = '5px';
    tog.css('background-color', bg)
        .css('border-width', '0px')
        .css('border-right', wid_new + ' ridge ' + bg)
        .css('cursor', 'pointer')
        .css('position', 'absolute')
        .css('left', '-' + wid_new)
        .css('top', '0px')
        .css('width', wid_new);
    bod.css('position', 'relative');
    bod.prepend(tog);

    /* Cookie helpers. */
    key = 'sphinxsidebar=';
    set_state = function (s) {
        var date = new Date();
        /* Expiry in 7 days. */
        date.setTime(date.getTime() + (7 * 24 * 3600 * 1000));
        document.cookie = key + encodeURIComponent(s) + '; expires=' +
            date.toUTCString() + '; path=/';
    };
    get_state = function () {
        var i, c, crumbs = document.cookie.split(';');
        for (i = 0; i < crumbs.length; i += 1) {
            c = crumbs[i].replace(/^\s+/, '');
            if (c.indexOf(key) === 0) {
                return decodeURIComponent(c.substring(key.length, c.length));
            }
        }
        return null;
    };

    /* Event handlers. */
    tog.mouseover(function (ev) {
        tog.css('border-right-color', fg);
    }).mouseout(function (ev) {
        tog.css('border-right-color', bg);
    }).click(function (ev) {
        if (bod.hasClass('wide')) {
            bod.removeClass('wide');
            bod.css('margin-left', wid_old);
            bar.css('width', wid_old);
            bar.show();
            set_state('visible');
        } else {
            set_state('hidden');
            bar.hide();
            bar.css('width', '0px');
            bod.css('margin-left', wid_new);
            bod.addClass('wide');
        }
    });

    /* Hide the normally visible sidebar? */
    if (get_state() === 'hidden') {
        tog.trigger('click');
    } else {
        set_state('visible');
    }
});
  </script>
  <script type="text/javascript">
/* detex the document title by removing "\(", "\)", "\", "$" */
document.title = document.title.replace(/\\\(/g, '').replace(/\\\)/g, '').replace(/\\/g, '').replace(/\$/g, '');
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66100-14', 'auto');
  ga('send', 'pageview');
</script>
  </body>
</html>